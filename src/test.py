from fastapi import BackgroundTasks
from core.models.card import Card
from core.models.user import User
from services import wallet_service, exchange_service, user_service, card_service, encryption_service


list_of_cards = [(1, encryption_service.encrypt_data('1234 5678 9101 1121'), '2025-05-01', encryption_service.encrypt_data('John'), encryption_service.encrypt_data('123'), 5000, 'BGN', None),
                    (2, encryption_service.encrypt_data('1234 5678 9101 1121'), '2025-05-02', encryption_service.encrypt_data('Alice'), encryption_service.encrypt_data('123'), 5000, 'BGN', None),
                    (3, encryption_service.encrypt_data('1234 5678 9101 1121'), '2025-05-03', encryption_service.encrypt_data('Tom'), encryption_service.encrypt_data('123'), 5000, 'BGN', None),
                    (4, encryption_service.encrypt_data('1234 5678 9101 1121'), '2025-05-04', encryption_service.encrypt_data('Gucci Boy'), encryption_service.encrypt_data('123'), 50000, 'USD', None),
                    (5, encryption_service.encrypt_data('1234 5678 9101 1121'), '2025-05-05', encryption_service.encrypt_data('Blagoy'), encryption_service.encrypt_data('123'), 5000, 'BGN', None)
                    ]


# for card in list_of_cards:
#     print(card)


encryted_list_of_cards = [(1, b'gAAAAABkf4PLG7uHc56p2b1SRHYvfmjWQL04aqXAEVUupgZvgfgqVOp5XK_S2dxNwy0aU2_N3Ef7ZFjscehW8XA4FJ-S6igTOi8toJtn_MVA5RCzK5PiMpY=', '2025-05-01', 
                           b'gAAAAABkf4PL9bSiOGDksc9zIWZdEsnjtDnoHIqILtKl4spEF1DeF-woSBFJ9M2SRtxQdr3gMjihdMePyW4CCSBnUu1LoqV-6g==', 
                           b'gAAAAABkf4PLgPzrC6D1G7v0qGZzZc9nZcWvs3geZCIArFfVsc5TOaXM8dygXnCjYr9yu4vFYDKxTaVmD-PjPYogKKmzR7Jj5Q==', 5000, 'BGN', None),
                          (2, b'gAAAAABkf4PLB7fZHNxDdVG0w_COh3jIyZhpol0pSn8o6Gm3ydxp32fmgkN3ivNGk_WVsHcoV6YLYfkJDUuRDrHioDchS0VA0SIztViDO8JC36H11Pt3zT4=', '2025-05-02', 
                           b'gAAAAABkf4PL7UqL6kz25R7bNmGZp4OiCJzSikMgC4AtBofNR6Oq5O7G-Y5C10SNiuyszCU9Y0QNJdnM0tiaYM-plEwMxMtUmA==', 
                           b'gAAAAABkf4PLGloBu7F6tYDqpT_ObrZZAvz7nxqguXGlvcEW1JFPO4Z-MlY5j9ZW1tgDfGZECHLQYGDDAOGCqZzcePG5E7c1og==', 5000, 'BGN', None),
                          (3, b'gAAAAABkf4PLvHQX7O72RwE4tgzEeep3-0wl6VwzOjwI4HCYfb9s9K4T6hLad2Moe52LjWDMs6nIelHWdKPZMc2IYt1NYBGnD11G9t9-MbvP5Q8CmzZwAhg=', '2025-05-03', 
                           b'gAAAAABkf4PLLVwYVjOr0mg85cfcJa3p8Yn9fD9kbUQ7sDKvGkG0WZ8gp3LccPkTzoWv25oPllWIF3M-yg_Yjwj_S813_l0reA==', 
                           b'gAAAAABkf4PLA9wZ_oDIj8dVjmlChW42RwL8C6yrGvnf_pShe1X6t-dgaH9fDihxvOmkldvMYXIj0ZTn4drl63rIE9RaUgZUMw==', 5000, 'BGN', None),
                          (4, b'gAAAAABkf4PLiW1DOwP2UPiAOeosWESTXl_QAjWURChrrCbsWk4ZKdn0OrUUL1tqRLfzviHp82yAday8jZtF4nqx5OhzdTlIcU7XerzCsvELzhvkpro9xQQ=', '2025-05-04', 
                           b'gAAAAABkf4PLQoA7CkWEKoBPqSUJcdU1L3hv0ksN2AprVrEIWciXl4C_KIlplr0Ig-0MF-MZWzTW6QwN2kB6VPtUk1btO8ywQw==', 
                           b'gAAAAABkf4PLBl-T4u2hPc61TsmpQduGEWaYOlAmvj9t3PNJF5pDzty9vKE1RH84AbD5ZEqsp9h7xxK1L1xJDK-Gf0QIQj58iQ==', 50000, 'USD', None),
                          (5, b'gAAAAABkf4PLkktGs4bOSlkjTj-lt-4BWTyKNsxkLr_NpHoFuiTep9DCledEpmPn2X5mZoiTU7z9YN6T_zzep9B6XLeYJbCCnif_LSwjfxV6Pzftei5ErQk=', '2025-05-05', 
                           b'gAAAAABkf4PLVwHIrVj_Ym9phwjVFZEqDnjeTykSjVcT3rE1lc_ghietmd59JRJifNTNmTnWUrj63pW7f1qkiiLPfVuixahSlg==', 
                           b'gAAAAABkf4PLP_W7LKqJRSkJF7-J4F94I4xClu0ohUhbNKjTjAo0mzzE8VmDaHS7tbW_Q8rjtXPg9Y5kKmTI6D84pYBEccRsoA==', 5000, 'BGN', None),
                          ]




data = [(5, b'gAAAAABkf4PLkktGs4bOSlkjTj-lt-4BWTyKNsxkLr_NpHoFuiTep9DCledEpmPn2X5mZoiTU7z9YN6T_zzep9B6XLeYJbCCnif_LSwjfxV6Pzftei5ErQk=', '2025-05-05', b'gAAAAABkf4PLVwHIrVj_Ym9phwjVFZEqDnjeTykSjVcT3rE1lc_ghietmd59JRJifNTNmTnWUrj63pW7f1qkiiLPfVuixahSlg==', b'gAAAAABkf4PLP_W7LKqJRSkJF7-J4F94I4xClu0ohUhbNKjTjAo0mzzE8VmDaHS7tbW_Q8rjtXPg9Y5kKmTI6D84pYBEccRsoA==', 5000, 'BGN', None)]
data1 = [(5, b'gAAAAABkf4PLkktGs4bOSlkjTj-lt-4BWTyKNsxkLr_NpHoFuiTep9DCledEpmPn2X5mZoiTU7z9YN6T_zzep9B6XLeYJbCCnif_LSwjfxV6Pzftei5ErQk=', '2025-05-05', b'gAAAAABkf4PLVwHIrVj_Ym9phwjVFZEqDnjeTykSjVcT3rE1lc_ghietmd59JRJifNTNmTnWUrj63pW7f1qkiiLPfVuixahSlg==', b'gAAAAABkf4PLP_W7LKqJRSkJF7-J4F94I4xClu0ohUhbNKjTjAo0mzzE8VmDaHS7tbW_Q8rjtXPg9Y5kKmTI6D84pYBEccRsoA==', 5000, 'BGN', None)]
id, value1, expiration, value2, value3, balance, currency, value4 = data[0]
print(id)
print(data == data1)