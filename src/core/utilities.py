import datetime
import hashlib
from fastapi import HTTPException
from starlette.requests import Request
from core.auth.jwt_bearer import JwtBearer
from core.database import read_query
import random
import string
import bcrypt

def get_current_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def get_future_time():
    future = datetime.datetime.now() + datetime.timedelta(seconds=120)
    return future.strftime("%Y-%m-%dT%H:%M:%S")

HASHING_ALGORITHM = 'SHA256'

def password_validator(password:str):
    special_chars = '!"#$%&\'()*+,-./:;<=>?@[]^_`{|}~'
    valid_length = False
    valid_capital = False
    valid_small = False
    valid_char = False
    valid_number = False
    for ch in password:
        if ch.isnumeric():
            valid_number = True
        elif ch.islower():
            valid_small = True
        elif ch.isupper():
            valid_capital = True
        elif ch in special_chars:
            valid_char = True
    if len(password) >= 8:
        valid_length = True
    return all([valid_length,
                valid_capital,
                valid_char,
                valid_number,
                valid_small,
                ])         




def check_token(request: Request):
    x_token = request.headers.get("Authorization")
    if not JwtBearer.verify_jwt(x_token):
        raise HTTPException(status_code=403, detail='Invalid or expired token')
    return x_token




def id_exists(id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT id FROM {table_name} where id = ?',
            (id,)))



def users_id_exists(users_id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT users_id FROM {table_name} where users_id = ?',
            (users_id,)))

def comments_id_exists(comments_id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT comments_id FROM {table_name} where comments_id = ?',
            (comments_id,)))


def create_hash(data: str) -> str:
    return bcrypt.hashpw(bytes(data, encoding='utf-8'), bcrypt.gensalt())

def compare_hash(new_hash:str, old_hash:str):
    return new_hash == old_hash
    
def paginate(data: list, separate: int=0):
    if separate == 0:
        return data
    result = []
    counter = 1
    for index, value in enumerate(data):
        next = (separate * counter)
        if index == next:
            result.append({'Page': counter
            })
            next += separate
            counter += 1
        result.append(value) 
    result.append({'Page': counter})       
    return result        

 
def get_page(data: list[dict], page: int=None):
    result = []
    start_at = False
    for d in data:
        if page == 1:
            result.append(d)
            if isinstance(d, dict) and d['Page'] == page:  
                break  
        if isinstance(d, dict) and d['Page'] == page - 1:
            start_at = True
            continue
        if start_at:
            result.append(d)
        if isinstance(d, dict) and d['Page'] == page:  
            break  
    return result 


def generate_random_code(length: int):
    characters = string.ascii_letters + string.digits
    code = ''.join(random.choice(characters) for _ in range(length))
    return code
