import jwt
# from jose import JWSError
from datetime import datetime, timedelta
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel
from passlib.context import CryptContext
from core.database import read_query


pass_crypt=CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/user/token')

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
TOKEN_EXPIRE_SEC = 6000

def password_hash(password: str):
    return pass_crypt.hash(password)

def verify(typed_pass: str, hashed_pass: str):
    return pass_crypt.verify(typed_pass, hashed_pass)

class user_in_DB(BaseModel):
    id: int
    username: str
    password: str
    email: str
    phone_number: str
    created_at: datetime | None
    is_blocked: bool | None
    is_admin: bool | None
    is_verified: bool | None
    default_wallet: int | None

    @classmethod
    def create_db_user(cls, 
                       id, 
                       username, 
                       password, 
                       email, 
                       phone_number, 
                       created_at, 
                       is_blocked, 
                       is_admin, 
                       is_verified,
                       default_wallet,
                       ):
        return cls(
            id=id,
            username=username,
            password=password,
            email=email,
            phone_number=phone_number,
            created_at=created_at,
            is_blocked=is_blocked,
            is_admin=is_admin,
            is_verified=is_verified,
            default_wallet=default_wallet,
            )

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenInfo(BaseModel):
    id: int
    username: str
    email:str
    is_admin: bool
    is_blocked: bool


def get_token(info: dict):
    to_encode = info.copy()
    expire = datetime.utcnow() + timedelta(seconds=TOKEN_EXPIRE_SEC)
    to_encode.update({"exp": expire})
    return jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)


def validate_token(token: str, login_error):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        id: int = payload.get("id")
        username: str = payload.get("username")
        email: str = payload.get("email")
        is_admin: bool = payload.get("is_admin")
        is_blocked: bool = payload.get("is_blocked")
        if not username:
            raise login_error
        token_info = TokenInfo(id=id, username=username,email=email,
                               is_admin=is_admin, is_blocked=is_blocked)
    except Exception:
        raise login_error
    return token_info


def get_current_user(token: str = Depends(oauth2_scheme),) -> TokenInfo:
    login_error = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Session expired please login again.",
        headers={"WWW-Authenticate": "Bearer"},
    )
    return validate_token(token, login_error)


def create_token(login_details: OAuth2PasswordRequestForm) -> Token:
    row = read_query(
        """SELECT * FROM users WHERE users.username = ?""", (login_details.username,))

    db_user = (user_in_DB.create_db_user(*row[0]) if row else None)

    if not db_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found in DB")

    if not verify(login_details.password, db_user.password):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="Log-in details not valid")

    the_token = get_token(
        {"id": db_user.id, 
         "username": db_user.username,
         "password": db_user.password,
         "email": db_user.email,
         "is_admin": db_user.is_admin, 
         "is_blocked": db_user.is_blocked}
    )
    return Token(access_token=the_token, token_type="bearer")
