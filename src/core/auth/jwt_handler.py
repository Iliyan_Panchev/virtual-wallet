import time
import jwt

JWT_SECRET = "cdf4356hgg3423dfd34tre"
JWT_ALGORITHM = "HS256"
TOKEN_EXPIRATION = 6000

def token_response(token: str):
    return {
        "access token": token
    }


def signJWT(user_id: int, 
            username: str, 
            email: str, 
            role: int, 
            status: int, 
            verified: int,
            ):
    payload = {
        "user_id": user_id,
        "username": username,
        "email": email,
        "is_admin": role,
        "is_blocked": status,
        "is_verified": verified,
        "expiry": time.time() + TOKEN_EXPIRATION
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token_response(token)


def decodeJWT(token: str):
    try:
        real_token = token.split(" ")[1]
        decode_token = jwt.decode(real_token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decode_token if decode_token['expiry'] >= time.time() else None
    except:
        return {}


def get_user_data(token: str, 
                  user_id:bool | None=None,
                  username:bool | None=None, 
                  email:bool | None=None, 
                  is_admin:bool | None=None, 
                  is_blocked:bool | None=None,
                  is_verified:bool | None=None,
                  ):
    token = token.split(' ')[1]
    decoded = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    if user_id:
        return decoded['user_id']
    elif username:
        return decoded['username']
    elif email:
        return decoded['email']
    elif is_admin:
        return decoded['is_admin']
    elif is_blocked:
        return decoded['is_blocked']
    elif is_verified:
        return decoded['is_verified']


