from mariadb import connect
from mariadb.connections import Connection
import mariadb
import sys

try:
    def _get_connection() -> Connection:
        return connect(
            user='root',
            password='root',
            host='127.0.0.1',
            port=3306,
            database='wallet',
        )

except mariadb.Error as err:
    print(f"Error connecting to MariaDB Platform: {err}")
    sys.exit(1)


def read_query(sql: str, sql_params=()):
    """returns empty list if there are no elements"""
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return cursor.fetchall()


def insert_query(sql: str, sql_params=()) -> int:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid


def update_query(sql: str, sql_params=()) -> bool:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount


def read_query_one(sql: str, sql_params=()):
    """returns None if there are no elements"""
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return cursor.fetchone()
    


def query_count(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return cursor.fetchone()

    