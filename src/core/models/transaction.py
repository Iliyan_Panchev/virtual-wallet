from datetime import datetime
from typing import Literal
from pydantic import BaseModel, constr


class Transaction(BaseModel):
    id: int | None
    information: constr(max_length=45) | None
    period: datetime | None
    recurring: int | None
    currency: str | None
    amount: float | None
    direction: str | None 
    closed: bool | None
    created: datetime | None
    sender: str | int | None
    receiver: str | int | None
    status: str | None


    @classmethod
    def from_query_result(cls,id, 
                          information, 
                          period, 
                          recurring,
                          currency, 
                          amount, 
                          closed, 
                          created, 
                          sender, 
                          receiver,
                          status):
        return cls(id=id,
                   information=information,
                   period=period,
                   recurring=recurring,
                   currency=currency,
                   amount=round(amount, 2),
                   closed=closed,
                   created=created,
                   sender=sender,
                   receiver=receiver,
                   status=status
                   )

class TransactionFilter(BaseModel):
    filter_by: Literal['recipient', 'period', 'status'] = None

    def __str__(self):
            return str(self.filter_by)
class TransactionSort(BaseModel):
    sort_by: Literal['amount', 'date'] = None

    def __str__(self):
        return str(self.sort_by)