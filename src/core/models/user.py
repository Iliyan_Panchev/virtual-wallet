from typing import Literal
from pydantic import BaseModel, constr, EmailStr
from datetime import datetime
from core.models.transaction import Transaction
from core.models.wallet import Wallet


class User(BaseModel):
    id: int | None
    username: constr(min_length=2, max_length=20) | None
    password: constr(min_length=8) | None
    email: EmailStr | None
    phone_number: constr(max_length=10) | None
    created_at: datetime | None
    is_blocked: bool | None 
    is_admin: bool | None 
    is_verified: bool | None 
    wallets: list[Wallet] = []
    transactions:list[Transaction] = []
    default_wallet: int | str | None
    
    @classmethod
    def from_query_result(cls, id, username, password, email, phone_number, created_at, is_blocked, is_admin, is_verified, default_wallet):
        return cls(
            id=id,
            username=username,
            password=password,
            email=email,
            phone_number=phone_number,
            created_at=created_at,
            is_blocked=is_blocked,
            is_admin=is_admin,
            is_verified=is_verified,
            default_wallet=default_wallet
            )


class UserLogin(BaseModel):
    email: str
    password: str


class CreateUserModel(BaseModel):
    username: constr(min_length=2, max_length=20)
    password: constr(min_length=8)
    email: EmailStr
    phone_number: constr(max_length=10)


class UserSearch(BaseModel):
    search_by: Literal['username', 'phone', 'email',] = None
    
    def __str__(self):
        return str(self.search_by)
    

class UserSort(BaseModel):
    sort_by: Literal['created', 'name', 'email'] = None

    def __str__(self):
        return str(self.sort_by)  


class SelectPassword(BaseModel):
    auto_generate_password: Literal['Yes', 'No'] = None 

    def __str__(self):
        return str(self.auto_generate_password)   

class UserTransactions(BaseModel):
    transactions: Literal['incoming', 'outgoing'] = None

    def __str__(self):
        return str(self.transactions)    
    
class FriendList(BaseModel):
    username: str
    email: str
    phone_number: str

    @classmethod
    def from_query_result(cls, username, email, phone_number):
        return cls(
            username=username,
            email=email,
            phone_number=phone_number,
            )
    

class UpdateUser(BaseModel):
    username: constr(min_length=2, max_length=20)
    password: constr(min_length=8)
    email: EmailStr
    phone_number: constr(max_length=10)