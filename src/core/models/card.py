from datetime import date
from pydantic import BaseModel, constr


class Card(BaseModel):
    id: int | None
    card_number: constr(max_length=19) | None
    expiration: date | None
    holder: constr(min_length=2, max_length=30) |None
    cvv: constr(strict=3) | None
    balance: float | None
    currency: str | None
    users_id: int | None


    @classmethod
    def from_query_result(cls, id, card_number, expiration, holder, cvv, balance, currency, user_id):
        return cls(
            id=id,
            card_number=card_number,
            expiration=expiration,
            holder=holder,
            cvv=cvv,
            balance=round(balance, 2),
            currency=currency,
            users_id=user_id
            )


class RegisterCard(BaseModel):
    card_number: constr(max_length=19)
    expiration: date
    holder: constr(min_length=2, max_length=30)
    cvv: constr(strict=3)
    balance: float 
    currency: str
