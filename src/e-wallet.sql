-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema wallet
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wallet
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wallet` DEFAULT CHARACTER SET latin1 ;
USE `wallet` ;

-- -----------------------------------------------------
-- Table `wallet`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(21) NULL DEFAULT NULL,
  `password` VARCHAR(250) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `phone_number` VARCHAR(10) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `is_blocked` TINYINT(4) NOT NULL,
  `is_admin` TINYINT(4) NOT NULL,
  `is_verified` TINYINT(4) NOT NULL,
  `default_wallet` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 56
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`cards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`cards` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `card_number` VARCHAR(256) NOT NULL,
  `expiration` DATE NOT NULL,
  `holder` VARCHAR(256) NOT NULL,
  `cvv` VARCHAR(256) NOT NULL,
  `balance` FLOAT NOT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `users_id`),
  INDEX `fk_Cards_Users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_Cards_Users`
    FOREIGN KEY (`users_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`transactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`transactions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `information` VARCHAR(45) NULL DEFAULT NULL,
  `period` DATETIME NULL DEFAULT NULL,
  `recurring` INT(11) NULL DEFAULT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `amount` FLOAT NULL DEFAULT NULL,
  `closed` TINYINT(4) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `sender_id` INT(11) NOT NULL,
  `receiver_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transactions_users1_idx` (`sender_id` ASC) VISIBLE,
  INDEX `fk_transactions_users2_idx` (`receiver_id` ASC) VISIBLE,
  CONSTRAINT `fk_transactions_users1`
    FOREIGN KEY (`sender_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_transactions_users2`
    FOREIGN KEY (`receiver_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 226
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  `transactions_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `transactions_id`, `users_id`),
  INDEX `fk_categories_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_categories_transactions1_idx` (`transactions_id` ASC) VISIBLE,
  CONSTRAINT `fk_categories_transactions1`
    FOREIGN KEY (`transactions_id`)
    REFERENCES `wallet`.`transactions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`contacts` (
  `users_id` INT(11) NOT NULL AUTO_INCREMENT,
  `friend_id` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`, `friend_id`),
  INDEX `fk_users_has_users_users2_idx` (`friend_id` ASC) VISIBLE,
  INDEX `fk_users_has_users_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_users_users2`
    FOREIGN KEY (`friend_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`referrals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`referrals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `friend_email` VARCHAR(45) NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `users_id`),
  INDEX `fk_referrals_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_referrals_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`wallets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`wallets` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `balance` FLOAT NULL DEFAULT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`users_has_wallets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`users_has_wallets` (
  `users_id` INT(11) NOT NULL,
  `wallets_id` INT(11) NOT NULL,
  `author_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`users_id`, `wallets_id`),
  INDEX `fk_users_has_wallets_wallets1_idx` (`wallets_id` ASC) VISIBLE,
  INDEX `fk_users_has_wallets_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_wallets_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `wallet`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_wallets_wallets1`
    FOREIGN KEY (`wallets_id`)
    REFERENCES `wallet`.`wallets` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`verification_codes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`verification_codes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `verification_code` VARCHAR(45) NULL DEFAULT NULL,
  `transactions_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `transactions_id`),
  UNIQUE INDEX `verification_code_UNIQUE` (`verification_code` ASC) VISIBLE,
  INDEX `fk_verification_codes_transactions1_idx` (`transactions_id` ASC) VISIBLE,
  CONSTRAINT `fk_verification_codes_transactions1`
    FOREIGN KEY (`transactions_id`)
    REFERENCES `wallet`.`transactions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wallet`.`wallets_has_transactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wallet`.`wallets_has_transactions` (
  `wallets_id` INT(11) NOT NULL,
  `transactions_id` INT(11) NOT NULL,
  PRIMARY KEY (`wallets_id`, `transactions_id`),
  INDEX `fk_wallets_has_transactions_transactions1_idx` (`transactions_id` ASC) VISIBLE,
  INDEX `fk_wallets_has_transactions_wallets1_idx` (`wallets_id` ASC) VISIBLE,
  CONSTRAINT `fk_wallets_has_transactions_transactions1`
    FOREIGN KEY (`transactions_id`)
    REFERENCES `wallet`.`transactions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wallets_has_transactions_wallets1`
    FOREIGN KEY (`wallets_id`)
    REFERENCES `wallet`.`wallets` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
