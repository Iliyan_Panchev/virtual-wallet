from fastapi import HTTPException, Request
from fastapi.templating import Jinja2Templates
from core.database import read_query, read_query_one, update_query, insert_query
from core.models.transaction import Transaction
from core.models.user import FriendList, User
from core.utilities import get_future_time, create_hash, compare_hash, get_current_time
from services import wallet_service

templates = Jinja2Templates(directory="src/scripts/html")

def get_all_registered_users(
        search: str | None=None,
        search_by: str | None=None,
        ):
    
    query = '''SELECT * FROM users '''
    if search_by == 'username':
        query += '''WHERE username LIKE ?'''
        users = read_query(query, (f'%{search}%',))
    elif search_by == 'phone': 
        query += '''WHERE phone_number LIKE ?'''
        users = read_query(query, (f'%{search}%',))
    elif search_by == 'email':
        query += '''WHERE email LIKE ?'''
        users = read_query(query, (f'%{search}%',))
    else:    
        users = read_query(query)

    return (User.from_query_result(*u) for u in users)



def sort_users(categories: list[User], *, attribute='id', reverse=False):
    if attribute == 'email':
        def sort_fn(u: User): return u.email   
    elif attribute == 'created':
        def sort_fn(u: User): return u.created_at
    elif attribute == 'name':
        def sort_fn(u: User): return u.username
    else:
        def sort_fn(u: User): return u.id

    return sorted(categories, key=sort_fn, reverse=reverse)


def get_profile_by_id(user_id: int, direction: str | None=None):
    user = read_query_one('''SELECT * FROM users WHERE id = ?''',(user_id,))
    user = User.from_query_result(*user)
    user.wallets = wallet_service.find_wallet_names(user_id)
    if direction == 'incoming':
        user.transactions = get_users_transactions_receiver(user_id)
    elif direction == 'outgoing':
        user.transactions = get_users_transactions_sender(user_id)
    else:
        user.transactions = get_users_transactions_receiver(user_id) + get_users_transactions_sender(user_id)
    user.default_wallet = wallet_service.find_user_wallet(user_id, name=True)    
    return user 


def get_user_by_id(user_id: int) -> User:
    user = read_query_one('''SELECT * FROM users where id = ?''', (user_id,))
    if not user:
        raise HTTPException(status_code=404, detail=f'User with id {user_id} not found.')
    return User.from_query_result(*user)


def get_users_transactions_receiver(user_id: int):
    transactions = read_query('''SELECT * FROM transactions WHERE receiver_id = ? ''', (user_id,))
    transactions = [Transaction.from_query_result(*t) for t in transactions]
    for t in transactions:
        t.direction = 'incoming'
    return transactions


def get_users_transactions_sender(user_id: int):
    transactions = read_query('''SELECT * FROM transactions WHERE sender_id = ?''', (user_id,))
    transactions = [Transaction.from_query_result(*t) for t in transactions]
    for t in transactions:
        t.direction = 'outgoing'
    return transactions



def create_user(user: User):
    generated_id = insert_query('''
    INSERT INTO users (username, password, email, phone_number, created, is_blocked, is_admin, is_verified)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                 (user.username, 
                  user.password, 
                  user.email, 
                  user.phone_number, 
                  get_current_time(), 
                  False, 
                  False,
                  False,
                  ))
    user.id = generated_id
    user.created_at = get_current_time()
    return user


def update_user(new_user: User, old_user: User) -> User:
    merged = User(
        id=old_user.id,
        username=new_user.username if new_user.username != old_user.username else old_user.username,
        password=new_user.password if new_user.password != old_user.password else old_user.password,
        email=new_user.email if new_user.email != old_user.email else old_user.email,
        phone_number=new_user.phone_number if new_user.phone_number != old_user.phone_number else old_user.phone_number,
        created_at=old_user.created_at,
        is_blocked=new_user.is_blocked,
        is_admin=new_user.is_admin,
        is_verified=new_user.is_verified
        )
    update_query('''UPDATE users SET 
    username = ?, password = ?, email = ?, phone_number = ?, created = ?, is_blocked = ? , is_admin = ?
    WHERE id = ?''',
                 (merged.username, 
                  merged.password, 
                  merged.email, 
                  merged.phone_number, 
                  merged.created_at, 
                  merged.is_blocked, 
                  merged.is_admin, 
                  merged.id))
    return merged



def delete_user(user_email: str):
    insert_query('''DELETE FROM users WHERE email = ?''',(user_email,))

    return {'msg': f'User with email {user_email} deleted.'}


def block_user(user_id:int):
    update_query('''UPDATE users SET is_blocked = ? 
    WHERE id = ?''', (True, user_id))
    
    return {'msg': f'User with id {user_id} blocked.'}


def unblock_user(user_id:int):
    update_query('''UPDATE users SET is_blocked = ? 
    WHERE id = ?''', (False, user_id))
    
    return {'msg': f'User with id {user_id} has been unblocked.'}


def is_blocked(user_id:int):
    result = read_query_one('''SELECT is_blocked FROM users WHERE id = ?''',(user_id,))

    return bool(result[0]) if result else None


def is_admin(user_id:int):
    result = read_query_one('''SELECT is_admin FROM users WHERE id = ?''',(user_id,))
    
    return bool(result[0]) if result else None


def is_verified(user_email: str):
    result = read_query_one('''SELECT is_verified FROM users WHERE email = ?''',(user_email,))
    
    return bool(result[0]) if result else None


def email_exists(email: str) -> bool:
    return any(
        read_query(
            f'SELECT email FROM users WHERE email = ?',
            (email,)))


def phone_exists(phone: str) -> bool:
    return any(
        read_query(
            f'SELECT phone_number FROM users WHERE phone_number = ?',
            (phone,)))

def add_friend(user_id: int,
               username: str | None=None, 
               phone_number: str | None=None, 
               email: str | None=None,
               ):
    if username:
        friend_id = wallet_service.find_user_by_username(username)
    elif phone_number:
        friend_id = wallet_service.find_user_by_phone(phone_number)
    elif email:
        friend_id = wallet_service.find_user_by_email(email)   

    insert_query('''INSERT INTO contacts(users_id, friend_id) VALUES(?, ?)''', (user_id, friend_id))         
    return {'message': 'Friend added.'}


def display_friends(user_id: int):
    friends_list = read_query('''SELECT U.username, U.email, U.phone_number FROM users as U
    JOIN contacts AS C ON C.friend_id = U.id
    WHERE C.users_id = ?''', (user_id,))
    if not friends_list:
        raise HTTPException(status_code=404, detail='Friend list is empty.')
    return (FriendList.from_query_result(*fr) for fr in friends_list)


def remove_friend(username: str | None=None, 
                  phone_number: str | None=None, 
                  email: str | None=None,
                  ):
    if username:
        friend_id = wallet_service.find_user_by_username(username)
    elif phone_number:
        friend_id = wallet_service.find_user_by_phone(phone_number)
    elif email:
        friend_id = wallet_service.find_user_by_email(email) 
    insert_query('''DELETE FROM contacts WHERE friend_id = ?''', (friend_id,))
    return {'message': 'Friend removed.'}


def compare_user_data(user_email:str, user_password: str):
    user_data = read_query_one('''SELECT password FROM users WHERE email = ?''', (user_email, ))
    hashes_password = create_hash(user_password)

    return compare_hash(hashes_password, user_data[0]) if user_data else None


def get_user_data_by_email(user_email: str) -> User:
    data = read_query_one('''SELECT * FROM users WHERE email = ?''', 
                      (user_email,))
    
    return User.from_query_result(*data)


def generate_verification_url(user_email: str):
    return f'http://127.0.0.1:8000/verification/registration/{user_email}/t/{get_future_time()}'

def generate_referral_url(user_email:str, friend_email:str):
    return f'http://127.0.0.1:8000/verification/user/{user_email}/friend/{friend_email}/t/{get_future_time()}'

def referral_redirect(request: Request, user_email: str):

    return templates.TemplateResponse('referral_redirect.html', {"request": request})

def verify_user_response(user_email, request: Request):
    insert_query('''UPDATE users SET is_verified = ? WHERE email = ?''',(True, user_email))

    return templates.TemplateResponse('thank_you.html', {'request': request})

def verification_url_expired_response(request:Request):
        return templates.TemplateResponse('thank_you_expired.html', {"request": request}, status_code=400)

def referral_url_expired_response(request: Request):
    return templates.TemplateResponse('referral_expired.html', {"request": request}, status_code=400)

def refer_friend_response(request: Request):
    return templates.TemplateResponse('register.html', {'request': request})


def home_page(request: Request):
    return templates.TemplateResponse('index.html', {'request': request})