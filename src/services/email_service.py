import smtplib
from email.message import EmailMessage
from core.database import insert_query, read_query_one, update_query
from core.models.transaction import Transaction
from core.utilities import get_current_time
from scripts.colour.colour_print import INFO_TEMPLATE
from services import transaction_service, wallet_service, exchange_service

signature = """
--
John Doe
Customer Support
TechPay | Silicon Valley, CA
Phone: (123) 456-7890
Email: customers@techpay.com
Website: www.techpay.com
"""


SENDER_EMAIL = 'e-wallet@abv.bg'
PASSWORD = '!7qn4q9NKYuGp_$'

def send_email(subject:str, receiver_email: str, body: str):
    message = EmailMessage()
    message.set_content(body + '\n' + signature)
    message["Subject"] = subject
    message["From"] = SENDER_EMAIL
    message["To"] = receiver_email

    with smtplib.SMTP_SSL("smtp.abv.bg", 465) as server:
        server.login(SENDER_EMAIL, PASSWORD)
        server.send_message(message)
    print(INFO_TEMPLATE + f'Email sent to {receiver_email} about {subject}')



def add_friend_referral(friend_email: str, user_id: int):
    insert_query('''INSERT INTO referrals (friend_email, users_id) VALUES (?, ?)''', (friend_email, user_id))


def user_is_referred(user_email: str):
    user_id = read_query_one('''SELECT users_id FROM referrals WHERE friend_email = ?''', (user_email,))
    return user_id[0] if user_id else None


def referral_bonus(wallet_id: int):
    amount = exchange_service.convert_currency(50, 'BGN', wallet_id=wallet_id)
    update_query(f'''UPDATE wallets SET balance = balance + {amount} WHERE id = ?''', (wallet_id,))
    user_id = wallet_service.find_user(wallet_id=wallet_id)
    transaction_service.create_transaction(Transaction(information='50 BGN referral bonus',
                                    period=get_current_time(),
                                    recurring=None,
                                    currency='BGN',
                                    amount=amount,
                                    direction=None,
                                    closed=False,
                                    created=get_current_time(),
                                    sender=user_id,
                                    receiver=user_id,
                                    status='pending'))