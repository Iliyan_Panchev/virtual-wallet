import datetime
from fastapi import HTTPException
from core.database import read_query, insert_query, update_query, query_count, read_query_one
from core.models.card import Card
from services import encryption_service

def get_all():
    data = read_query("SELECT * FROM cards")
    
    cards =(Card.from_query_result(c[0],
                                    encryption_service.decrypt_data(c[1]),
                                    c[2],
                                     encryption_service.decrypt_data(c[3]),
                                      encryption_service.decrypt_data(c[4]),
                                      c[5],
                                      c[6],
                                      c[7]) for c in data)
    return cards


def sort_cards(transactions: list[Card], *, attribute='id', reverse=False):
    if attribute == 'expiration':
        def sort_fn(c: Card): return c.expiration
    elif attribute == 'balance':
        def sort_fn(c: Card): return c.balance
    elif attribute == 'holder':
        def sort_fn(c: Card): return c.holder    
    else:
        def sort_fn(c: Card): return c.id

    return sorted(transactions, key=sort_fn, reverse=reverse)


def get_by_id(id: int, user_id: int):
    data = read_query("SELECT * FROM cards WHERE id = ?", (id,))
    if not data:
        raise HTTPException(status_code=404, detail=f"Card with id {id} doesnt exist")
    if not card_belongs_user(user_id, id):
        raise HTTPException(status_code=403,detail='You are trying to access another user\'s card.')
    id, number, expiration, holder, cvv, balance, currency, users_id  = data[0]
    card = Card.from_query_result(id,
                                  encryption_service.decrypt_data(number),
                                  expiration,
                                  encryption_service.decrypt_data(holder),
                                  encryption_service.decrypt_data(cvv),
                                  balance,
                                  currency,
                                  users_id,
                                  )
    return card


def register(card: Card, user_id):    
    card_id = insert_query("INSERT INTO cards (card_number, expiration, holder, cvv, balance, currency, users_id) "
                           "VALUES(?,?,?,?,?,?,?)",
                           (encryption_service.encrypt_data(card.card_number), 
                            card.expiration, 
                            encryption_service.encrypt_data(card.holder), 
                            encryption_service.encrypt_data(card.cvv), 
                            card.balance,
                            card.currency, 
                            user_id))
    card.id = card_id
    return card


def delete(id: int, user_id: int):
    data = read_query("SELECT * FROM cards WHERE id = ?", (id,))
    if not data:
        raise HTTPException(status_code=404, detail=f"Card with id {id} doesn\'t exist")
    if not card_belongs_user(user_id, id):
        raise HTTPException(status_code=403, detail='You are trying to delete another user\'s card.')    
    insert_query("DELETE FROM cards WHERE id = ?", (id,))
    return f"Card with id {id} successfully removed"


def update_card(new_card: Card, old_card: Card, user_id: int):
    merged = Card(
        id=old_card.id,
        card_number=new_card.card_number ,
        expiration=new_card.expiration ,
        holder=new_card.holder ,
        cvv=new_card.cvv ,
        balance=new_card.balance ,
        currency=new_card.currency,
        users_id=user_id
    )
    
    update_query("UPDATE cards SET card_number = ?, expiration = ?, holder = ?, cvv = ?, balance = ?, "
                 "users_id = ? WHERE id = ?",
                 (encryption_service.encrypt_data(merged.card_number), 
                  merged.expiration, 
                  encryption_service.encrypt_data(merged.holder), 
                  encryption_service.encrypt_data(merged.cvv), 
                  merged.balance,
                  merged.users_id, 
                  merged.id))
    return merged



def card_belongs_user(user_id: int, card_id):
    user_cards = read_query('''SELECT * FROM cards WHERE users_id = ? AND id = ?''',
                            (user_id, card_id))
    return bool(user_cards)


def find_user_card(user_id: int):
    """
    Finds users card with the most balance available.
    """
    card_id = read_query_one('''SELECT id FROM cards WHERE users_id = ? AND 
    balance = (SELECT MAX(balance) FROM cards WHERE users_id = ?)''', (user_id, user_id))
    if not card_id:
        raise HTTPException(status_code=404, detail='User has no cards registered.')
    return card_id[0]


def is_balance_sufficient(amount: str, card_id: int):
    return bool(query_count('''SELECT * FROM cards WHERE balance > ? and id = ?''',(amount, card_id)))


def is_card_expired_from_db(card_id: int):
    card_expiraton = read_query('''SELECT expiration FROM cards WHERE id = ?''', (card_id,))
    print(card_expiraton)
    if not card_expiraton:
        raise HTTPException(status_code=404, detail='Card expiration not found.')
    return datetime.date.today() > datetime.datetime.strptime(str(card_expiraton[0][0]), "%Y-%m-%d").date()
    