from fastapi import HTTPException
from core.database import insert_query, read_query, read_query_one, update_query, query_count
from core.models.transaction import Transaction
from apscheduler.schedulers.background import BackgroundScheduler
from core.utilities import get_current_time, id_exists
from scripts.colour.colour_print import INFO_TEMPLATE
from services import card_service, wallet_service, exchange_service

"""
Users must be able to view a list of their transactions filtered by period, recipient, and direction (incoming or outgoing) 
and sort them by amount and date. Transaction list should support pagination.
"""


def sort_transactions(transactions: list[Transaction], *, attribute='id', reverse=False):
    if attribute == 'date':
        def sort_fn(t: Transaction): return t.created
    elif attribute == 'amount':
        def sort_fn(t: Transaction): return t.amount
    else:
        def sort_fn(t: Transaction): return t.id

    return sorted(transactions, key=sort_fn, reverse=reverse)


def get_all_transactions(filtering: str, filter_by:str | None=None):
    if filter_by == 'period' and filtering:
        filtered_transactions = read_query('''SELECT * FROM transactions WHERE ? >= period''', (filtering,))
    elif filter_by == 'status' and filtering:
        filtered_transactions = read_query('''SELECT * FROM transactions WHERE status = ?''',(filtering,))
    elif filter_by == 'recipient' and filtering:
        filtered_transactions = read_query(
            '''SELECT T.id, T.information, T.period, T.currency, T.amount, T.direction, T.closed, T.created, T.sender_id, T.receiver_id 
               FROM transactions AS T
               JOIN users AS U ON U.id = T.receiver_id
               WHERE U.username LIKE ?''', (f'%{filtering}%',))  
    else:
        filtered_transactions = read_query('''SELECT * FROM transactions''')       
        
    return (Transaction.from_query_result(*t) for t in filtered_transactions)


def get_transaction_by_id(transaction_id: int):
    transaction = read_query_one('''SELECT * FROM transactions WHERE id = ?''', (transaction_id,))

    return Transaction.from_query_result(*transaction)



def delete_transaction(transaction_id: int):
    insert_query("DELETE FROM transactions WHERE id = ?", (transaction_id,))

    return {'message':f'Transaction with id {transaction_id} deleted.'}


def create_transaction(transaction: Transaction):
    generated_id = insert_query('''
    INSERT INTO transactions 
    (information, period, recurring, currency, amount, closed, created, sender_id, receiver_id, status) 
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
    (transaction.information, 
     transaction.period, 
     transaction.recurring, 
     transaction.currency,
     transaction.amount,
     transaction.closed,
     transaction.created,
     transaction.sender,
     transaction.receiver,
     transaction.status,
     ))
    transaction.id = generated_id

    return transaction.id


def create_recurring_transaction(recurring_period: int, information:str, amount: str, receiver_id:int, wallet_id: int | None=None):
    if not id_exists(receiver_id, 'users'):
        raise HTTPException(status_code=400, detail=f'User with id {receiver_id} not found.')
    if not card_service.is_balance_sufficient(amount, wallet_id):
            raise HTTPException(status_code=400, detail='Insufficient funds or no wallet available.')
    if wallet_id:
        if not wallet_service.is_balance_sufficient(amount, wallet_id):
            raise HTTPException(status_code=400, detail='Insufficient wallet balance or missing card.')
        user_id = wallet_service.find_user(wallet_id=wallet_id)
        currency = exchange_service.check_wallet_currency(wallet_id)
    transaction_id = create_transaction(Transaction(information=information,
                                                    period=get_current_time(),
                                                    recurring=recurring_period,
                                                    currency=currency,
                                                    amount=amount,
                                                    direction=None,
                                                    closed=False,
                                                    created=get_current_time(),
                                                    sender=user_id,
                                                    receiver=receiver_id,
                                                    status='recurring'))    
    insert_query('''INSERT INTO wallets_has_transactions (wallets_id, transactions_id) VALUES (?, ?)''',(wallet_id, transaction_id))
    return f'Recurring transaction created for {amount} {currency} every {recurring_period} days'


def open_transaction_listener():
    '''
    The function is scheduled to run every 45 secs and check for opened transactions
    who's period has passed the current date and subsequently closes them marking them as finished.
    '''
    if query_count('''SELECT * FROM transactions LIMIT 1''') is not None:
        transaction_ids = read_query(
            '''SELECT id FROM transactions WHERE closed != 1 AND NOW() >= period AND recurring IS NULL''')
        if transaction_ids:
            transaction_ids = ','.join([str(id) for id in list(map(lambda x: x[0], transaction_ids))])
            insert_query(f'''UPDATE transactions SET closed = ? WHERE id IN ({transaction_ids})''', (True,))
            print(INFO_TEMPLATE + f'Transaction {transaction_ids} marked as complete.')
            

def recurring_transaction_listener():
    '''
    The function is scheduled to run every 120 secs and check for recurring transactions.
    If such is found the current transaction is maked as closed and new one is created with updated period.
    Up to 720 recurring transactions can be processed for 24 hrs.
    '''
    recurring_transactions_ids = read_query('''SELECT id FROM transactions WHERE NOW() > period AND recurring IS NOT NULL AND closed = False''')
    if recurring_transactions_ids:
        withdraw_recurring_transaction(recurring_transactions_ids)
        create_next_recurring()
        closed_old_recurring()

        
    
scheduler = BackgroundScheduler()
scheduler.add_job(open_transaction_listener, 'interval', seconds=45)
scheduler.add_job(recurring_transaction_listener, 'interval', seconds=120)


def withdraw_recurring_transaction(recurring_transactions_ids: list[tuple]):
    recurring_transactions_ids = ','.join([str(id) for id in list(map(lambda x: x[0], recurring_transactions_ids))])
    insert_query(f'''UPDATE wallets
    JOIN wallets_has_transactions ON wallets.id = wallets_has_transactions.wallets_id
    JOIN transactions ON wallets_has_transactions.transactions_id = transactions.id
    SET wallets.balance = wallets.balance - transactions.amount
    WHERE transactions.id IN ({recurring_transactions_ids});
                    ''')
    print(INFO_TEMPLATE + f'Recurring transaction {recurring_transactions_ids} updated.')

def create_next_recurring():
    insert_query('''
    INSERT INTO transactions (information, period, recurring, currency, amount, closed, created, sender_id, receiver_id, status)
    SELECT information, DATE_ADD(period, INTERVAL recurring DAY), recurring, currency, amount, closed, created, sender_id, receiver_id, "recurring"
    FROM (
    SELECT information, period, recurring, currency, amount, closed, created, sender_id, receiver_id, status
    FROM transactions
    WHERE recurring IS NOT NULL AND NOW() > period AND closed != 1
    ) AS temp;
    ''')

def closed_old_recurring():
    update_query('''UPDATE transactions SET closed = TRUE, status="accepted" 
        WHERE recurring IS NOT NULL AND NOW() > period''')

