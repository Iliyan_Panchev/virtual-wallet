from fastapi import HTTPException
from core.database import query_count, read_query, insert_query, read_query_one, update_query
from core.models.transaction import Transaction
from core.models.wallet import Wallet
from core.utilities import id_exists, generate_random_code, get_current_time
from services import card_service, email_service, exchange_service , transaction_service
# from services.transaction_service import delete_transaction, create_transaction


def get_all():
    data = read_query("SELECT * FROM wallets")

    wallets = (Wallet.from_query_result(*x) for x in data)
    return wallets


def get_by_id(id: int):
    data = read_query_one("SELECT * FROM wallets WHERE id = ?", (id,))
    if not data:
        raise HTTPException(status_code=404, detail=f"Wallet with id {id} doesnt exist")
    wallet = Wallet.from_query_result(*data)
    return wallet


def create(currency: str, name: str, user_id: int):
    wallet_id = insert_query("INSERT INTO wallets(name, balance, currency, created) "
                             "VALUES(?,?,?,?)", (name, 0, currency, get_current_time()))
    insert_query("INSERT INTO users_has_wallets(users_id, wallets_id, author_id) "
                 "VALUES(?, ?, ?)", (user_id, wallet_id, user_id))
    update_query("UPDATE users SET default_wallet = ? WHERE id = ?", (wallet_id, user_id))

    return f"Wallet successfully created"


def delete(id: int):
    data = read_query("SELECT * FROM wallets WHERE id = ?", (id,))
    if data is None:
        raise HTTPException(status_code=404, detail=f"Wallet with id {id} doesnt exist")

    insert_query("DELETE FROM wallets WHERE id = ?", (id,))
    return f"Wallet with id {id} successfully deleted"


def add_money_to_wallet(wallet_id: int, amount: float, card_id: int):
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} doesnt exist")
    if not id_exists(card_id, "cards"):
        raise HTTPException(status_code=404, detail=f"Card with id {card_id} doesnt exist")
    if card_service.is_card_expired_from_db(card_id):
        raise HTTPException(status_code=400, detail='Card is expired.')
    if not card_service.is_balance_sufficient(amount, card_id):
        raise HTTPException(status_code=400,detail='Insufficient funds or no card available.')
    card_currency = exchange_service.check_card_currency(card_id)
    wallet_currency = exchange_service.check_wallet_currency(wallet_id)
    # Update card balance
    update_query(f"UPDATE cards SET balance = balance - {amount} WHERE id = ?", (card_id,))
    # Calculate currency rate
    amount = exchange_service.convert_currency(amount,  card_currency, wallet_id=wallet_id)
    # Update wallet balance
    update_query(f"UPDATE wallets SET balance = balance + {amount} WHERE id = ?", (wallet_id,))
    transaction_service.create_transaction(Transaction(information='sending money to wallet', period=get_current_time(), 
                                                    recurring=None, currency=wallet_currency,
                                                    amount=amount, 
                                                    direction='outgoing', closed=False, 
                                                    created=get_current_time(), sender=find_user(card_id=card_id), 
                                                    receiver=find_user(wallet_id=wallet_id), status='accepted'))    
    return f"{amount} {wallet_currency} successfully added to wallet"


def remove_money_from_wallet(wallet_id: int, amount: float, card_id: int):
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} doesnt exist")
    if not id_exists(card_id, "cards"):
        raise HTTPException(status_code=404, detail=f"Card with id {card_id} doesnt exist")
    if not is_balance_sufficient(amount, wallet_id):
        raise HTTPException(status_code=400,detail='Insufficient funds or no wallet available.')
    if card_service.is_card_expired_from_db(card_id):
        raise HTTPException(status_code=400, detail='Card is expired.')

    card_currency = exchange_service.check_card_currency(card_id)
    wallet_currency = exchange_service.check_wallet_currency(wallet_id)

    # Update wallet balance
    update_query(f"UPDATE wallets SET balance = balance - {amount} WHERE id = ?", (wallet_id,))
    # Update card balance
    amount = exchange_service.convert_currency(amount, wallet_currency, card_id=card_id)
    update_query(f"UPDATE cards SET balance = balance + {amount} WHERE id = ?", (card_id,))
    transaction_service.create_transaction(Transaction(information='withdrawing money from wallet', period=get_current_time(), 
                                                    recurring=None, currency=card_currency,
                                                    amount=amount, 
                                                    direction='outgoing', closed=False, 
                                                    created=get_current_time(), sender=find_user(card_id=card_id), 
                                                    receiver=find_user(wallet_id=wallet_id), status='accepted'))    
    return f"Withdraw {amount} {card_currency} - Successful."


def check_wallet_access(user_id: int):
    wallets = read_query("SELECT * FROM wallets "
                         "JOIN users_has_wallets ON wallets.id = users_has_wallets.wallets_id"
                         "WHERE users_id = ?", (user_id,))
    if not wallets:
        raise HTTPException(status_code=404, detail="You must create wallet first")
    return f"Choose wallet:\n" \
           f"wallets: {[Wallet.from_query_result(*wallet) for wallet in wallets]}"



def send_confirmation(transaction_id: int):
    update_query("UPDATE transactions SET status = ? WHERE id = ?", ("pending", transaction_id))

    receiver_email = read_query("SELECT email FROM users "
                                "JOIN transactions ON users.id = transactions.receiver_id "
                                "WHERE transactions.id = ?", (transaction_id,))
    # SEND EMAIL TO RECEIVER LOGIC
    email_service.send_email("Someone is trying to send you money: You must accept or decline it", receiver_email[0][0],
                            f"accept: http://127.0.0.1:8000/transactions/accept_transaction/{transaction_id}\n"
                            f"decline: http://127.0.0.1:8000/transactions/decline_transaction/{transaction_id}")
    return "Waiting for receiver to accept the transaction"


def accept_trans(transaction_id: int):
    # Update transaction status
    update_query("UPDATE transactions SET status = ? WHERE id = ?", ("accepted", transaction_id))

    # Retrieve wallet_id
    sender_id = read_query("SELECT sender_id FROM transactions WHERE id = ?", (transaction_id,))
    sender_default_wallet_id = read_query("SELECT default_wallet FROM users WHERE id = ?", (sender_id[0][0],))[0][0]
    if not sender_default_wallet_id:
       sender_default_wallet_id = find_user_wallet(sender_id[0][0])
    # Insert id`s in many-to-many table wallets_has_transactions
    insert_query('''DELETE FROM wallets_has_transactions WHERE wallets_id = ? and transactions_id = ?''', 
                 (sender_default_wallet_id, transaction_id))
    insert_query("INSERT INTO wallets_has_transactions(wallets_id, transactions_id) "
                 "VALUES(?,?)", (sender_default_wallet_id, transaction_id))

    receiver = read_query("SELECT amount, receiver_id FROM transactions WHERE id = ?", (transaction_id,))
    # Retrieve receiver wallet
    receiver_wallet_id = read_query("SELECT wallets_id FROM users_has_wallets WHERE users_id = ?", (receiver[0][1],))

    # Take money from sender`s wallet
    update_query(f"UPDATE wallets SET balance = balance - {receiver[0][0]} WHERE id = ?", (sender_default_wallet_id,))
    wallet_currency = exchange_service.check_wallet_currency(sender_default_wallet_id)
    amount = exchange_service.convert_currency(receiver[0][0], wallet_currency, wallet_id=receiver_wallet_id[0][0])

    # Add money to receiver`s wallet
    update_query(f"UPDATE wallets SET balance = balance + {amount} WHERE id = ?", (receiver_wallet_id[0][0],))
    update_query('UPDATE transactions SET closed = 1 WHERE id = ?',(transaction_id,))

    return f"Transaction successful !"


def decline_trans(transaction_id: int):
    # Update transaction status
    update_query("UPDATE transactions SET status = ? WHERE id = ?", ("declined", transaction_id))
    return "Transaction is declined"


def send_verification_code(sender_email: str, transaction_id:int):
    verification_code = generate_random_code(30)
    insert_query('''INSERT INTO verification_codes(verification_code, transactions_id) VALUES(?, ?)''', (verification_code, transaction_id))
    email_service.send_email("Use this code to verify your transaction", sender_email, verification_code)
    return f"Verification code is sent to {sender_email}"


def confirm_verification_code(transaction_id, verification_code: str):
    code_from_db = read_query_one("SELECT verification_code FROM verification_codes "
                              "WHERE transactions_id = ?", (transaction_id,))
    if code_from_db[0] != verification_code:
        return "Incorrect verification code"
    else:
        receiver_email = read_query('''SELECT email from users 
                                    JOIN transactions ON transactions.receiver_id = users.id  
                                    WHERE transactions.id = ?''', (transaction_id,))

        email_service.send_email("Someone is trying to send u money: U must accept or decline it", receiver_email[0][0],
                                 f"accept: http://127.0.0.1:8000/transactions/accept_transaction/{transaction_id}\n"
                                 f"decline: http://127.0.0.1:8000/transactions/decline_transaction/{transaction_id}")
        return "Confirmation Email sent to receiver."


def send_to_phone_number(information:str, wallet_id: int, user_id: int, phone_number: str, amount: float, transaction_id: int | None=None):
    if transaction_id:
        transaction_service.delete_transaction(transaction_id)
    # Check if wallet exist
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} doesnt exist")
    if not is_balance_sufficient(amount, wallet_id):
            raise HTTPException(status_code=400,detail='Insufficient funds or no wallet available.')
    # Retrieve receiver id by phone number
    # receiver_id = read_query("SELECT id FROM users WHERE phone_number = ?", (phone_number,))
    receiver_id = find_user_by_phone(phone_number)
    print(receiver_id)
    # Check if user with that id exist
    if not receiver_id:
        raise HTTPException(status_code=404, detail="Incorrect phone number")
    currency = exchange_service.check_wallet_currency(wallet_id)
    # Insert transaction with pending status
    transaction_id = transaction_service.create_transaction(Transaction(information=information, period=get_current_time(), 
                                                    recurring=None, currency=currency,
                                                    amount=amount, 
                                                    direction='outgoing', closed=False, 
                                                    created=get_current_time(), sender=user_id, 
                                                    receiver=receiver_id[0][0], status='pending'))
    if amount >= 10000:
        sender_email = read_query("SELECT email FROM users WHERE id = ?", (user_id,))
        return {
            'transaction': transaction_id,
            'verification code': send_verification_code(sender_email[0][0], transaction_id)
        }

    return {"phone number": phone_number,
            "amount": amount,
            "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
            "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
            }


def send_to_username(information:str, wallet_id: int, user_id: int, username: str, amount: float, transaction_id: int | None = None):
    if transaction_id:
        transaction_service.delete_transaction(transaction_id)
    # Check if wallet exist
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} doesnt exist")
    if not is_balance_sufficient(amount, wallet_id):
            raise HTTPException(status_code=400,detail='Insufficient funds or no wallet available.')
    # Retrieve receiver id by phone number
    # receiver_id = read_query("SELECT id FROM users WHERE username = ?", (username,))
    receiver_id = find_user_by_username(username)
    # Check if user with that id exist
    if not receiver_id:
        raise HTTPException(status_code=404, detail="Incorrect username")
    currency = read_query("SELECT currency FROM wallets WHERE id = ?", (wallet_id,))
    transaction_id = transaction_service.create_transaction(Transaction(information=information, period=get_current_time(), 
                                    recurring=None, currency=currency[0][0],
                                    amount=amount, 
                                    direction='outgoing', closed=False, 
                                    created=get_current_time(), sender=user_id, 
                                    receiver=receiver_id[0][0], status='pending'))    
    if amount >= 10000:
        sender_email = read_query("SELECT email FROM users WHERE id = ?", (user_id,))
        return send_verification_code(sender_email[0][0], transaction_id)

    return {"username": username,
            "amount": amount,
            "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
            "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
            }


def send_to_email(information: str, wallet_id: int, user_id: int, email: str, amount: float, transaction_id: int | None = None):
    if transaction_id:
        transaction_service.delete_transaction(transaction_id)
    # Check if wallet exist
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} doesnt exist")
    if not is_balance_sufficient(amount, wallet_id):
            raise HTTPException(status_code=400,detail='Insufficient funds or no wallet available.')
    # Retrieve receiver id by phone number
    # receiver_id = read_query("SELECT id FROM users WHERE email = ?", (email,))
    receiver_id = find_user_by_email(email)
    # Check if user with that id exist
    if not receiver_id:
        raise HTTPException(status_code=404, detail="Incorrect email")
    currency = read_query("SELECT currency FROM wallets WHERE id = ?", (wallet_id,))
    transaction_id = transaction_service.create_transaction(Transaction(information=information, period=get_current_time(), 
                                    recurring=None, currency=currency[0][0],
                                    amount=amount, 
                                    direction='outgoing', closed=False, 
                                    created=get_current_time(), sender=user_id, 
                                    receiver=receiver_id[0][0], status='pending'))      
    if amount >= 10000:
        sender_email = read_query("SELECT email FROM users WHERE id = ?", (user_id,))
        return send_verification_code(sender_email[0][0], transaction_id)

    return {"email": email,
            "amount": amount,
            "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
            "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
            }


def add_user(wallet_id: int, author_id: int, phone_number: str | None = None, username: str | None = None, email: str | None = None):
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} does not exist.")
    if phone_number:
        # user_id = read_query("SELECT id FROM users WHERE phone_number = ?", (phone_number,))
        user_id = find_user_by_phone(phone_number)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with phone number {phone_number} not found.')
        insert_query("INSERT INTO users_has_wallets(users_id, wallets_id, author_id) "
                     "VALUES(?,?,?)", (user_id[0][0], wallet_id, author_id))
        return f"User with phone_number {phone_number} successfully added  to wallet with id {wallet_id}"
    elif username:
        # user_id = read_query("SELECT id FROM users WHERE username = ?", (username,))
        user_id = find_user_by_username(username)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with username {username} not found.')
        insert_query("INSERT INTO users_has_wallets(users_id, wallets_id, author_id) "
                     "VALUES(?,?,?)", (user_id[0][0], wallet_id, author_id))
        return f"User {username} successfully added  to wallet with id {wallet_id}"
    elif email:
        # user_id = read_query("SELECT id FROM users WHERE email = ?", (email,))
        user_id = find_user_by_email(email)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with email {email} not found.')
        insert_query("INSERT INTO users_has_wallets(users_id, wallets_id, author_id) "
                     "VALUES(?,?,?)", (user_id[0][0], wallet_id, author_id))
        return f"User with email: {email} successfully added  to wallet with id {wallet_id}"
    else:
        return "Something is wrong"


def revoke_access(wallet_id: int, author_id: int, phone_number: str | None = None, username: str | None = None, email: str | None= None):
    if not id_exists(wallet_id, "wallets"):
        raise HTTPException(status_code=404, detail=f"Wallet with id {wallet_id} does not exist.")
    author_from_db = read_query("SELECT * FROM users_has_wallets "
                                "WHERE wallets_id = ? AND author_id = ?", (wallet_id, author_id))
    if not author_from_db:
        raise HTTPException(status_code=400, detail="You are not the creator of this wallet")
    if phone_number:
        # user_id = read_query("SELECT id FROM users WHERE phone_number = ?", (phone_number,))
        user_id = find_user_by_phone(phone_number)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with phone number {phone_number} not found.')
        insert_query("DELETE FROM users_has_wallets WHERE users_id = ? AND wallets_id = ?", (user_id[0][0], wallet_id))
        return f"User with phone_number {phone_number} successfully deleted  to wallet with id {wallet_id}"
    elif username:
        # user_id = read_query("SELECT id FROM users WHERE username = ?", (username,)).
        user_id = find_user_by_username(username)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with username {username} not found.')
        insert_query("DELETE FROM users_has_wallets WHERE users_id = ? and wallets_id = ?", (user_id[0][0], wallet_id))
        return f"User {username} successfully deleted to wallet with id {wallet_id}"
    elif email:
        # user_id = read_query("SELECT id FROM users WHERE email = ?", (email,))
        user_id = find_user_by_email(email)
        if not user_id:
            raise HTTPException(status_code=404, detail=f'User with phone email {email} not found.')
        insert_query("DELETE FROM users_has_wallets WHERE users_id = ? AND wallets_id = ?", (user_id[0][0], wallet_id))
        return f"User with email: {email} successfully deleted to wallet with id {wallet_id}"
    else:
        return "Something is wrong"


def is_balance_sufficient(amount: float, wallet_id: int):
    return bool(query_count('''SELECT * FROM wallets WHERE balance > ? and id = ?''',(amount, wallet_id)))


def find_user_wallet(user_id: int, name: bool=False):
    """
    Get users default wallet or find the wallet with the most balance.\n
    Raise exception if no wallet is find.
    """
    wallet_id = read_query_one('''SELECT default_wallet FROM users WHERE id = ?''', (user_id,))
    if wallet_id[0] == None:
        wallet_id = read_query_one('''SELECT id FROM wallets 
        WHERE balance = (
        SELECT MAX(balance) 
        FROM wallets 
        JOIN users_has_wallets AS UW ON UW.wallets_id = wallets.id
        WHERE users_id = ?)''', (user_id,))
    if wallet_id == None:
        raise HTTPException(status_code=404, detail='User has no available wallets.')
    if name:
        wallet_name = read_query_one('''SELECT name FROM wallets WHERE id = ?''', (wallet_id[0],))
        return wallet_name[0] 
    return wallet_id[0]


def find_wallet_names(user_id: int):
    names = read_query('''SELECT name FROM wallets
        JOIN users_has_wallets AS UW ON UW.wallets_id = wallets.id
        WHERE UW.users_id = ?''',(user_id,))
    return [str(id) for id in list(map(lambda x: x[0], names))]


def find_user(card_id: int | None=None, 
              wallet_id: int | None=None):
    """
    Input card or wallet id from the system, receive users id.
    """
    if card_id:
        user_id = read_query_one('''SELECT users.id FROM users 
        JOIN cards ON cards.users_id = users.id
        WHERE cards.id = ?''', (card_id,))
        return user_id[0]
    elif wallet_id:
        user_id = read_query_one('''SELECT users.id FROM users
        JOIN users_has_wallets AS UW ON UW.users_id = users.id
        JOIN wallets AS W ON W.id = UW.wallets_id
        WHERE wallets_id = ?''', (wallet_id,))
        return user_id[0]
    raise HTTPException(status_code=404, detail='User does not exist.')


def is_users_wallet(user_id: int):
    """Short and concise finds if the user has any wallets"""
    return bool(query_count('''SELECT wallets_id FROM users_has_wallets 
    JOIN users as U ON U.id = users_has_wallets.users_id
    WHERE U.id = ?''', (user_id,)))


def find_user_by_phone(phone_number: str):
    receiver_id = read_query_one("SELECT id FROM users WHERE phone_number = ?", (phone_number,))
    if not receiver_id:
        raise HTTPException(status_code=403, detail=f'User with phone {phone_number} not found.')
    return receiver_id[0]


def find_user_by_username(username: str):
    receiver_id = read_query_one("SELECT id FROM users WHERE username = ?", (username,))
    if not receiver_id:
        raise HTTPException(status_code=403, detail=f'User with username {username} not found.')
    return receiver_id[0]


def find_user_by_email(email: str):
    receiver_id = read_query_one("SELECT id FROM users WHERE email = ?", (email,))
    if not receiver_id:
        raise HTTPException(status_code=403, detail=f'User with email {email} not found.')
    return receiver_id[0]