import requests
from core.database import read_query_one
from scripts.colour.colour_print import INFO_TEMPLATE

FREECURRENCY_API = "https://api.freecurrencyapi.com/v1/latest?apikey=mIACslJCeNTinTy6L6StbkVUTfJYYK26SfnXKMhT&currencies=EUR%2CUSD&base_currency=BGN"

def request_exchange_rate(currency: str='EUR'):
    try:
        if currency == 'BGN':
            return 1
        """
        currently works for base currency BGN
        """
        if currency.upper() not in ('USD', 'EUR'):
            return {
                'message': 'Only USD and EUR supported.'
            }
        response = requests.get(FREECURRENCY_API)
        if response.status_code == 200:
            data = response.json()
            print(INFO_TEMPLATE + 'Exchange made for ' + currency)
            return data['data'][currency]
        else:
            return{"Error": response.status_code}
    except Exception:
        return 1    


def check_card_currency(card_id: int):
    card_currency = read_query_one('''SELECT currency FROM cards WHERE id = ?''', (card_id,))   
    return card_currency[0]


def check_wallet_currency(wallet_id: int):
    wallet_currency = read_query_one('''SELECT currency FROM wallets WHERE id = ?''', (wallet_id,))
    return wallet_currency[0]


def convert_currency(amount: float,
                    from_currency: str, 
                    wallet_id: int | None=None, 
                    card_id: int | None=None,
                    ):
    if wallet_id:
        wallet_currency = check_wallet_currency(wallet_id)
        return _exchange_currency(amount, from_currency, wallet_currency)
    elif card_id:
        card_currency = check_card_currency(card_id)   
        return _exchange_currency(amount, from_currency, card_currency)
    
    return amount    


def _exchange_currency(amount: float, from_currency: str, to_currency: str):

    if from_currency == "BGN":
        amount /= request_exchange_rate('BGN')
    elif from_currency == "EUR":
        amount /= request_exchange_rate('EUR')
    elif from_currency == "USD":
        amount /= request_exchange_rate('USD')

    if to_currency == "BGN":
        amount *= request_exchange_rate('BGN')
    elif to_currency == "EUR":
        amount *= request_exchange_rate('EUR')
    elif to_currency == "USD":
        amount *= request_exchange_rate('USD')

    return amount