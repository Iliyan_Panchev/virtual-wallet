from cryptography.fernet import Fernet

def generate_key():
    key = Fernet.generate_key()
    with open("src/encryption_key.txt", "wb") as key_file:
        key_file.write(key)

def load_key():
    return 'pZ4Q7EhwqL0gkJ446vQEBih7DN_BUGuV4idFcHTdWAk='

def encrypt_data(message):
    f = Fernet(key)
    encrypted_message = f.encrypt(message.encode())
    return encrypted_message

def decrypt_data(encrypted_message):
    f = Fernet(key)
    decrypted_message = f.decrypt(encrypted_message).decode()
    return decrypted_message

# generate_key()

key = load_key()

