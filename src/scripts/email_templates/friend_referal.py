from services import user_service
from core.auth.jwt_handler import get_user_data


def refer_friend_email(user_email: str, friend_email: str ):
    referral_url = user_service.generate_referral_url(user_email, friend_email)
    user = user_service.get_user_data_by_email(user_email)
    return f'''Hello there,

    I hope this email finds you well. I wanted to share an exciting opportunity with you to join our e-wallet community and enjoy the benefits of a secure and convenient digital wallet.
    {user.username} has referred you to our e-wallet, and we're thrilled to invite you to join us. With our e-wallet, you can easily manage your finances, make quick payments, and enjoy various features designed to simplify your daily transactions.
    Here's how you can get started:
    1. Click on the following referral link to access our e-wallet registration page: {referral_url}
    2. Complete the registration process by providing your necessary information. It's quick, easy, and free!
    3. Once you've successfully registered and logged in, you'll be able to explore the full range of features and benefits our e-wallet offers.
    By joining our e-wallet through {user.username}'s referral, you're not only gaining access to a secure digital wallet but also giving {user.username} the opportunity to earn exciting rewards through our referral program. It's a win-win!
    If you have any questions or need assistance during the registration process, our dedicated support team is here to help. Feel free to reach out to us at support@techpay.com or call us at (123) 456 789.
    We're thrilled to welcome you to our e-wallet community and look forward to providing you with a seamless and rewarding digital payment experience.

    Best regards,

    {user.username}

    '''

SUBJECT_FRIEND_REFERRAL = 'You\'ve been referred to our e-wallet!'