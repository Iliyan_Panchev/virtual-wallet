from services import user_service

def registration_verification_email(username: str, user_email: str):
    verification_url = user_service.generate_verification_url(user_email)
    return f'''Dear {username},

    Thank you for registering with TechPay. We are excited to have you join our platform and take advantage 
    of our convenient and secure e-wallet services. 
    To ensure the security of your account, we require you to verify your registration.
    Please follow the steps below to complete your account verification,
    click the link bellow:
    👇  👇  👇  👇  👇  👇 
    <{verification_url}>
    '''

SUBJECT_VERIFY_USER = 'Verify Your Account Registration for Virtual Wallet'
