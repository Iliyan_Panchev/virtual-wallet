import random
from faker import Faker
     
  
class DebitCard:
    def __init__(self, 
                  card_number: str,
                  expiration_date: str, 
                  cardholder_name: str, 
                  cvv: str, bank_name: str, 
                  balance: float
                  ):
        
        self.card_number = card_number
        self.expiration_date = expiration_date
        self.cardholder_name = cardholder_name
        self.cvv = cvv
        self.bank_name = bank_name
        self.balance = balance
  
    # def deposit(self, amount: float):
    #     self.balance += amount
        
    # def withdraw(self, amount: float):
    #     if amount > self.balance:
    #         raise ValueError("Insufficient balance")
    #     self.balance -= amount
    def print_card_details(self):
        return {
        f'Card number: {self.card_number}',
        f'Expiration: {self.expiration_date}',
        f'Cardholder: {self.cardholder_name}',
        f'CVV: {self.cvv}',
        f'Bank: {self.bank_name}',
        f'Balance: {self.balance}',
        }  



class CardGenerator:
    """Dummy card generator"""
    @staticmethod
    def _generate_number():
        card = ''
        digits = "0123456789"
        result = [random.choice(digits) for _ in range(20)]
        for i, ch in enumerate(result):
            if i % 5 == 0:
                card += ' '
            else:
                card += ch   
        return card[1:]
    
    @staticmethod
    def _generate_expiration():
        month = random.randint(1, 12)
        year = random.randint(23, 30)
        return f'{month}/{year}'

    @staticmethod
    def _generate_name():
        fake = Faker()
        return fake.name()

    @staticmethod
    def _generate_cvv():
        return random.randint(100, 999)
        
    @staticmethod
    def _generate_bank():
        fake = Faker()
        bank_name: str = fake.name().split()[-1]
        if 'a' in bank_name.lower():
            return 'Bank of Python'
        return f'Bank of {bank_name}'
    
    @staticmethod
    def _generate_balance():
        return f'{float(random.randint(500, 15000)):.2f}'
    
    @staticmethod
    def generate_card(): 
        card = DebitCard(
            CardGenerator._generate_number(), 
            CardGenerator._generate_expiration(), 
            CardGenerator._generate_name(), 
            CardGenerator._generate_cvv(), 
            CardGenerator._generate_bank(), 
            CardGenerator._generate_balance(),
            )
        
        return card

print(CardGenerator.generate_card().print_card_details())
