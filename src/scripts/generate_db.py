from core.database import insert_query, query_count


# create_db_script = ''

# with open('\src\e-wallet.sql', 'rt') as file:
#     create_db_script = file.read()
        

# print(create_db_script)



    # data seed
if query_count('SELECT COUNT(*) from transactions') == 0:
    print('Inserting transactions')
    insert_query('''INSERT INTO transactions (information, period, currency, amount, direction, closed, created, sender_id, receiver_id) VALUES
                ('gucci money', 
                '2025-04-11', 
                'BGN', '1250', 
                'outgoing', 
                'True', 
                '2025-04-03 22:11:00', 
                '3', 
                '4')
                ''')

    # if query_count('SELECT COUNT(*) from devs') == 0:
    #     print('Inserting devs')
    #     insert_query('''INSERT INTO devs(name,level) VALUES
    #                         ('Gosho Petrov',2),
    #                         ('Petar Georgiev',1),
    #                         ('Stamat Todorov',3)''')

    # if query_count('SELECT COUNT(*) from devs_projects') == 0:
    #     print('Inserting devs into projects')
    #     insert_query('''INSERT INTO devs_projects(dev_id,project_id) VALUES
    #                         (1,2),
    #                         (2,2),
    #                         (3,2),
    #                         (3,1)''')
