$("#registerForm").submit(function (event) {
    event.preventDefault();

    var user_data = {
        username: $("#username").val(),
        email: $("#email").val(),
        password: $("#password").val(),
        phone: $("#phone").val()
    };

    $.ajax({
        type: "POST",
        url: "http://127.0.0.1:8000/user/register",
        data: JSON.stringify(user_data),
        contentType: 'application/json',
        success: function (result) {
            alert("Registration successful!");
            // Handle the success response as needed
        },
        error: function () {
            alert("There was an error during registration!");
            // Handle the error response as needed
        }
    });
});
