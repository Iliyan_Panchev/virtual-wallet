import unittest
from unittest.mock import patch
from core.models.transaction import Transaction
from core.utilities import get_current_time
from src.services import transaction_service
from unittest.mock import Mock

mock_transaction_service = Mock('src.core.database.read_query_one')
transaction_service.get_transaction_by_id = mock_transaction_service

LIST_OF_TRANSACTIONS = [
    Transaction.from_query_result(*(1,'Money Laundering', get_current_time(), None, 'BGN', 100_000_000, True, '2025-05-01 00:00:00', 1, 2, 'accepted')),
    Transaction.from_query_result(*(1,'Internet', get_current_time(), None, 'BGN', 10, True, '2025-05-02 00:00:00', 1, 2, 'accepted')),
    Transaction.from_query_result(*(1,'Gift', get_current_time(), None, 'BGN', 150, True, '2025-05-03 00:00:00', 1, 2, 'accepted')),
    Transaction.from_query_result(*(1,'Versace Bag', get_current_time(), None, 'BGN', 3500, True, '2025-05-04 00:00:00', 1, 2, 'accepted')),
    Transaction.from_query_result(*(1,'Salary Transfer', get_current_time(), None, 'BGN', 1500, True, '2025-05-05 00:00:00', 1, 2, 'accepted')),
]


class SortTransactionsShould(unittest.TestCase):

    def test_sort_transactions_returnsSortedByDateAscending(self):

        result = transaction_service.sort_transactions(LIST_OF_TRANSACTIONS, attribute='date', reverse=False)

        self.assertEqual(LIST_OF_TRANSACTIONS, result)



    def test_sort_transactions_returnsSortedByDateDescending(self):
        expected = LIST_OF_TRANSACTIONS[::-1]
        result = transaction_service.sort_transactions(LIST_OF_TRANSACTIONS, attribute='date', reverse=True)

        self.assertEqual(expected, result)


    def test_sort_transactions_returnsSortedByAmount(self):
        expected = sorted(LIST_OF_TRANSACTIONS, key=lambda t: t.amount, reverse=True)
        
        result = transaction_service.sort_transactions(LIST_OF_TRANSACTIONS, attribute='amount', reverse=True)

        self.assertEqual(expected, result)


    def test_sort_transactions_returnsSortedByDefault(self):

        result = transaction_service.sort_transactions(LIST_OF_TRANSACTIONS, reverse=True)

        self.assertEqual(LIST_OF_TRANSACTIONS, result)


# class GetTransactionByIdShould(unittest.TestCase):
#     @patch('src.services.transaction_service.read_query_one')
#     def test_get_transaction_by_id_returnsCorrectTransaction(self, mock_db_read_query_one):
#             mock_db_read_query_one.return_value = (1, 'Salary Transfer', get_current_time(), None, 'BGN', 1500, True, '2025-05-05 00:00:00', 1, 2, 'accepted')

#             result = transaction_service.get_transaction_by_id(1)

#             self.assertEqual(result, LIST_OF_TRANSACTIONS[4])
