import unittest
from unittest import mock
from unittest.mock import patch
from fastapi import HTTPException
from core.models.transaction import Transaction
from services import wallet_service
from core.models.wallet import Wallet


class WalletShould(unittest.TestCase):

    @patch('services.wallet_service.read_query')
    def test_return_all_wallets(self, mock_db_read_query):
        mock_db_read_query.return_value = [(1, "Gucci Wallet", 1000.00, "BGN", None),
                                           (2, "My Wallet", 2000.00, "BGN", None),
                                           (3, "Friends Wallet", 3000.00, "BGN", None),
                                           (4, "Main Wallet", 4000.00, "BGN", None)]

        expected = [Wallet.from_query_result(*(1, "Gucci Wallet", 1000.00, "BGN", None)),
                                           Wallet.from_query_result(*(2, "My Wallet", 2000.00, "BGN", None)),
                                           Wallet.from_query_result(*(3, "Friends Wallet", 3000.00, "BGN", None)),
                                           Wallet.from_query_result(*(4, "Main Wallet", 4000.00, "BGN", None))]

        result = wallet_service.get_all()

        self.assertEqual(expected, list(result))

    @patch('services.wallet_service.read_query')
    def test_return_empty_list(self, mock_db_read_query):
        mock_db_read_query.return_value = []
        expected = []
        result = wallet_service.get_all()
        self.assertEqual(expected, list(result))


    @patch('services.wallet_service.read_query_one')
    def test_return_wallet_by_id(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = (1, "Gucci Wallet", 1000.00, "BGN", None)
        expected = Wallet.from_query_result(*(1, "Gucci Wallet", 1000.00, "BGN", None))
        result = wallet_service.get_by_id(1)
        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_raise_http_exception(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = []
        id = -1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.get_by_id(id)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {id} doesnt exist")

    @patch('services.wallet_service.insert_query')
    @patch('services.wallet_service.update_query')
    def test_return_create_wallet(self, mock_db_insert_query, mock_db_update_query):
        mock_db_insert_query.return_value = None
        mock_db_update_query.return_value = 1
        expected = "Wallet successfully created"
        result = wallet_service.create("BGN", "Wallet 1", 1)
        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.insert_query')
    def test_return_delete_wallet(self, mock_db_read_query, mock_db_insert_query):
        id = 1
        mock_db_read_query.return_value = None
        mock_db_insert_query.return_value = [(1, "Gucci Wallet", 1000.00, "BGN", None)]

        expected = f"Wallet with id {id} successfully deleted"
        result = wallet_service.delete(id)
        self.assertEqual(expected, result)

    @patch('services.wallet_service.insert_query')
    @patch('services.wallet_service.read_query')
    def test_raise_http_exception(self, mock_db_read_query, mock_db_insert_query):
        mock_db_read_query.return_value = None
        mock_db_insert_query.return_value = []
        id = -1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.delete(id)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {id} doesnt exist")


    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_wallet_id_doesnt_exist(self, mock_id_exist):
        mock_id_exist.return_value = None
        wallet_id = -1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_money_to_wallet(wallet_id, 1000, 1)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    def test_raise_HTTPException_when_card_id_doesnt_exist(self, mock_id_exist, mock_is_card_expired_from_db):
        mock_id_exist.return_value = 1
        mock_is_card_expired_from_db.return_value = False
        card_id = 1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_money_to_wallet(1, 1000, card_id)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Card is expired.')

    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    @patch('services.card_service.is_balance_sufficient')
    def test_raise_HTTPException_when_balance_sufficient(self, mock_id_exist, mock_is_card_expired_from_db,
                                                           mock_is_balance_sufficient):
        mock_id_exist.return_value = 1
        mock_is_card_expired_from_db.return_value = True
        mock_is_balance_sufficient.return_value = False
        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_money_to_wallet(1, 1000, 1)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Insufficient funds or no card available.')

    @patch('services.wallet_service.find_user')
    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    @patch('services.card_service.is_balance_sufficient')
    @patch('services.exchange_service.check_card_currency')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.wallet_service.update_query')
    @patch('services.exchange_service.convert_currency')
    @patch('services.transaction_service.create_transaction')
    def test_add_money_to_wallet(self, mock_create_transaction, mock_convert_currency, mock_update_query,
                                 mock_check_wallet_currency, mock_check_card_currency,
                                 mock_is_balance_sufficient, mock_is_card_expired_from_db,
                                 mock_id_exists, mock_find_user):
        # Mock the dependencies
        mock_id_exists.return_value = True
        mock_is_card_expired_from_db.return_value = False
        mock_is_balance_sufficient.return_value = True
        mock_check_card_currency.return_value = 'USD'
        mock_check_wallet_currency.return_value = 'EUR'
        mock_update_query.return_value = None
        mock_create_transaction.return_value = None
        mock_find_user.return_value = 1
        mock_convert_currency.return_value = 1000.0

        wallet_id = 1
        amount = 1000.0
        card_id = 123
        expected = "1000.0 EUR successfully added to wallet"
        # Call the method under test
        result = wallet_service.add_money_to_wallet(wallet_id, amount, card_id)
        # Assert the expected behavior
        self.assertEqual(expected, result)


    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_wallet_id_doesnt_exist(self, mock_id_exist):
        mock_id_exist.return_value = None
        wallet_id = -1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.remove_money_from_wallet(wallet_id, 1000, 1)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    def test_raise_HTTPException_when_card_id_doesnt_exist(self, mock_id_exist, mock_is_card_expired_from_db):
        mock_id_exist.return_value = 1
        mock_is_card_expired_from_db.return_value = False
        card_id = 1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.remove_money_from_wallet(1, 1000, card_id)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Card is expired.')

    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    @patch('services.card_service.is_balance_sufficient')
    def test_raise_HTTPException_when_balance_sufficient(self, mock_id_exist, mock_is_card_expired_from_db,
                                                           mock_is_balance_sufficient):
        mock_id_exist.return_value = 1
        mock_is_card_expired_from_db.return_value = True
        mock_is_balance_sufficient.return_value = False
        with self.assertRaises(HTTPException) as exception:
            wallet_service.remove_money_from_wallet(1, 1000, 1)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Insufficient funds or no card available.')

    @patch('services.wallet_service.query_count')
    @patch('services.wallet_service.find_user')
    @patch('services.wallet_service.id_exists')
    @patch('services.card_service.is_card_expired_from_db')
    @patch('services.card_service.is_balance_sufficient')
    @patch('services.exchange_service.check_card_currency')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.wallet_service.update_query')
    @patch('services.exchange_service.convert_currency')
    @patch('services.transaction_service.create_transaction')
    def test_remove_money_to_wallet(self, mock_create_transaction, mock_convert_currency, mock_update_query,
                                 mock_check_wallet_currency, mock_check_card_currency,
                                 mock_balance_sufficient, mock_is_card_expired_from_db,
                                 mock_id_exists, mock_find_user, mock_db_query_count):
        # Mock the dependencies
        mock_id_exists.return_value = True
        mock_db_query_count.return_value = 1
        mock_is_card_expired_from_db.return_value = False
        mock_balance_sufficient.return_value = True
        mock_check_card_currency.return_value = 'USD'
        mock_check_wallet_currency.return_value = 'EUR'
        mock_update_query.return_value = None
        mock_create_transaction.return_value = None
        mock_find_user.return_value = 1
        mock_convert_currency.return_value = 1000.0

        wallet_id = 1
        amount = 1000.0
        card_id = 123
        expected = "Withdraw 1000.0 USD - Successful."
        # Call the method under test
        result = wallet_service.remove_money_from_wallet(wallet_id, amount, card_id)
        # Assert the expected behavior
        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query')
    def test_raise_HTTPException_when_wallet_id_doesnt_exist(self, mock_db_read_query):
        mock_db_read_query.return_value = []
        user_id = -1
        with self.assertRaises(HTTPException) as exception:
            wallet_service.check_wallet_access(user_id)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, "You must create wallet first")

    @patch('services.wallet_service.read_query')
    def test_return_check_user_wallets(self, mock_db_read_query):
        mock_db_read_query.return_value = [(1, "Gucci Wallet", 1000.00, "BGN", None),
                                           (2, "My Wallet", 2000.00, "BGN", None),
                                           (3, "Friends Wallet", 3000.00, "BGN", None),
                                           (4, "Main Wallet", 4000.00, "BGN", None)]
        user_id = 1
        wallets = [(1, "Gucci Wallet", 1000.00, "BGN", None),
                   (2, "My Wallet", 2000.00, "BGN", None),
                   (3, "Friends Wallet", 3000.00, "BGN", None),
                   (4, "Main Wallet", 4000.00, "BGN", None)]

        expected = f"Choose wallet:\n" \
           f"wallets: {[Wallet.from_query_result(*wallet) for wallet in wallets]}"

        result = wallet_service.check_wallet_access(user_id)

        self.assertEqual(expected, result)


    @patch('services.wallet_service.update_query')
    @patch('services.wallet_service.read_query')
    @patch('services.email_service.send_email')
    def test_returns_send_confirmation(self, mock_send_email, mock_db_read_query, mock_db_update_query):
        mock_db_read_query.return_value = "gosho@abv.bg"
        mock_db_update_query.return_value = 1
        mock_send_email.return_value = None

        expected = "Waiting for receiver to accept the transaction"
        transaction_id = 5
        result = wallet_service.send_confirmation(transaction_id)

        self.assertEqual(expected, result)

    # @patch('services.wallet_service.update_query')
    # @patch('services.wallet_service.read_query')
    # @patch('services.wallet_service.insert_query')
    # @patch('services.exchange_service.check_wallet_currency')
    # @patch('services.exchange_service.convert_currency')
    # def test_return_correct_msg_accept_trans(self, mock_convert_currency, mock_check_wallet_currency,
    #                                          mock_db_insert_query, mock_db_read_query, mock_db_update_query):
    #     mock_convert_currency.return_value = 1000
    #     mock_check_wallet_currency.return_value = "EUR"
    #     mock_db_insert_query.return_value = None
    #     mock_db_read_query.return_value = 1
    #     mock_db_update_query.return_value = None
    #
    #     transaction_id = 1
    #     expected = "Transaction successful !"
    #
    #     result = wallet_service.accept_trans(transaction_id)
    #
    #     self.assertEqual(expected, result)

    @patch('services.wallet_service.update_query')
    def test_returns_correct_msg(self, mock_db_update_query):
        mock_db_update_query.return_value = None

        expected = "Transaction is declined"

        result = wallet_service.decline_trans(5)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.generate_random_code')
    @patch('services.wallet_service.insert_query')
    @patch('services.email_service.send_email')
    def test_returns_correct_msg_for_verification_code(self, mock_send_email, mock_db_insert_query,
                                                       mock_generate_random_code):
        mock_generate_random_code.return_value = "asfsdgsfgs"
        mock_db_insert_query.return_value = None
        mock_send_email.return_value = None

        sender_email = "vladoka357@gmail.com"
        transaction_id = 1

        expected = f"Verification code is sent to {sender_email}"

        result = wallet_service.send_verification_code(sender_email, transaction_id)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    @patch('services.wallet_service.read_query')
    @patch('services.email_service.send_email')
    def test_returns_currect_msg_for_confirm_verfication_code(self, mock_send_email, mock_db_read_query,
                                                              mock_db_read_query_one):
        mock_db_read_query_one.return_value = "sdgsfhdgd",
        mock_db_read_query.return_value = "gosho@abv.bg"
        mock_send_email.return_value = None

        verification_code = "sdgsfhdgd"
        transaction_id = 1

        expected = "Confirmation Email sent to receiver."

        result = wallet_service.confirm_verification_code(transaction_id, verification_code)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_return_correct_msg_when_verification_code_incorrect(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = "asdsdf",

        verification_code = "aasdsasdad"
        transaction_id = 1

        expected = "Incorrect verification code"

        result = wallet_service.confirm_verification_code(transaction_id, verification_code)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_id_doesnt_exist(self, mock_id_exists):
        mock_id_exists.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_phone_number("Credit", wallet_id, 1, "0891231234", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    def test_raise_HTTPException_when_id_balance_is_sufficient_exist(self, mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_phone_number("Credit", wallet_id, 1, "0891231234", 1000)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Insufficient funds or no wallet available.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_phone')
    def test_raise_HTTPException_when_phone_number_doesnt_exist(self, mock_find_user_by_phone, mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_phone.return_value = None
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_phone_number("Credit", wallet_id, 1, "0891231234", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, "Incorrect phone number")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_phone')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.transaction_service.create_transaction')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.send_verification_code')
    def test_returns_correct_msg_when_amount_is_higher_thank_10k(self, mock_send_verification_code, mock_db_read_query, mock_create_transaction,
                                                                 mock_check_wallet, mock_find_user_by_phone,
                                                                 mock_balance_sufficient, mock_id_exists):
        wallet_id = 1
        transaction_id = 5
        sender_email = "gosho@abv.bg"

        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_phone.return_value = [(5,)]
        mock_check_wallet.return_value = "BGN"
        mock_create_transaction.return_value = 5
        mock_db_read_query.return_value = "gosho@abv.bg"
        mock_send_verification_code.return_value = f"Verification code is sent to {sender_email}"



        expected = {
                'transaction': transaction_id,
                'verification code': f"Verification code is sent to {sender_email}"
            }

        result = wallet_service.send_to_phone_number("Credit", wallet_id, 1, "0891231234", 100000)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_phone')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.transaction_service.create_transaction')
    def test_returns_correct_msg_when_amount_is_lower_thank_10k(self,
                                                                 mock_create_transaction,
                                                                 mock_check_wallet, mock_find_user_by_phone,
                                                                 mock_balance_sufficient, mock_id_exists):


        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_phone.return_value = [(5,)]
        mock_check_wallet.return_value = "BGN"
        mock_create_transaction.return_value = 5

        wallet_id = 1
        transaction_id = 5
        phone_number = "0891231234"
        amount = 1000

        expected = {"phone number": phone_number,
                "amount": amount,
                "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
                "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
                }

        result = wallet_service.send_to_phone_number("Credit", wallet_id, 1, phone_number, amount)

        self.assertEqual(expected, result)

    # Send by username
    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_id_doesnt_exist_in_send_by_username(self, mock_id_exists):
        mock_id_exists.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_username("Credit", wallet_id, 1, "gosho123", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    def test_raise_HTTPException_when_id_balance_is_sufficient_exist_in_send_by_username(self, mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_username("Credit", wallet_id, 1, "gosho123", 1000)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Insufficient funds or no wallet available.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_username')
    def test_raise_HTTPException_when_username_doesnt_exist(self, mock_find_user_by_username, mock_balance_sufficient,
                                                                mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_username.return_value = None
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_username("Credit", wallet_id, 1, "0891231234", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, "Incorrect username")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_username')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.transaction_service.create_transaction')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.send_verification_code')
    def test_returns_correct_msg_when_amount_is_higher_thank_10k_in_send_to_username(self, mock_send_verification_code, mock_db_read_query,
                                                                 mock_create_transaction,
                                                                 mock_check_wallet, mock_find_user_username,
                                                                 mock_balance_sufficient, mock_id_exists):
        wallet_id = 1
        transaction_id = 5
        sender_email = "gosho@abv.bg"

        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_username.return_value = [(5,)]
        mock_check_wallet.return_value = "BGN"
        mock_create_transaction.return_value = 5
        mock_db_read_query.return_value = "gosho@abv.bg"
        mock_send_verification_code.return_value = f"Verification code is sent to {sender_email}"

        expected = f"Verification code is sent to {sender_email}"

        result = wallet_service.send_to_username("Credit", wallet_id, 1, "0891231234", 100000)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_username')
    @patch('services.transaction_service.create_transaction')
    @patch('services.wallet_service.read_query')
    def test_returns_correct_msg_when_amount_is_lower_thank_10k_in_send_by_username(self,mock_db_read_query,
                                                                mock_create_transaction,
                                                                mock_find_user_by_username,
                                                                mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_username.return_value = [(5,)]
        mock_db_read_query.return_value = [("BGN", )]
        mock_create_transaction.return_value = 5

        wallet_id = 1
        transaction_id = 5
        username = "gosho123"
        amount = 1000

        expected = {"username": username,
            "amount": amount,
            "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
            "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
            }

        result = wallet_service.send_to_username("Credit", wallet_id, 1, username, amount)

        self.assertEqual(expected, result)


    # Send by email
    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_id_doesnt_exist_in_send_by_email(self, mock_id_exists):
        mock_id_exists.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_email("Credit", wallet_id, 1, "gosho123@abv.bg", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    def test_raise_HTTPException_when_id_balance_is_sufficient_exist_in_send_by_email(self, mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = False
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_email("Credit", wallet_id, 1, "gosho123@abv.bg", 1000)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, 'Insufficient funds or no wallet available.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_email')
    def test_raise_HTTPException_when_email_doesnt_exist(self, mock_find_user_by_email, mock_balance_sufficient,
                                                                mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_email.return_value = None
        wallet_id = 1

        with self.assertRaises(HTTPException) as exception:
            wallet_service.send_to_email("Credit", wallet_id, 1, "gosho@abv.bg", 1000)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, "Incorrect email")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_email')
    @patch('services.exchange_service.check_wallet_currency')
    @patch('services.transaction_service.create_transaction')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.send_verification_code')
    def test_returns_correct_msg_when_amount_is_higher_thank_10k_in_send_to_email(self, mock_send_verification_code, mock_db_read_query,
                                                                 mock_create_transaction,
                                                                 mock_check_wallet, mock_find_user_username,
                                                                 mock_balance_sufficient, mock_id_exists):
        wallet_id = 1
        transaction_id = 5
        sender_email = "gosho123@abv.bg"

        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_username.return_value = [(5,)]
        mock_check_wallet.return_value = "BGN"
        mock_create_transaction.return_value = 5
        mock_db_read_query.return_value = "gosho@abv.bg"
        mock_send_verification_code.return_value = f"Verification code is sent to {sender_email}"

        expected = f"Verification code is sent to {sender_email}"

        result = wallet_service.send_to_email("Credit", wallet_id, 1, "gosho@abv.bg", 100000)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.is_balance_sufficient')
    @patch('services.wallet_service.find_user_by_email')
    @patch('services.transaction_service.create_transaction')
    @patch('services.wallet_service.read_query')
    def test_returns_correct_msg_when_amount_is_lower_thank_10k_in_send_by_email(self,mock_db_read_query,
                                                                mock_create_transaction,
                                                                mock_find_user_by_email,
                                                                mock_balance_sufficient, mock_id_exists):
        mock_id_exists.return_value = True
        mock_balance_sufficient.return_value = True
        mock_find_user_by_email.return_value = [(5,)]
        mock_db_read_query.return_value = [("BGN", )]
        mock_create_transaction.return_value = 5

        wallet_id = 1
        transaction_id = 5
        email = "gosho123@abv.bg"
        amount = 1000

        expected = {"email": email,
            "amount": amount,
            "accept": f"127.0.0.1:8000/wallets/{wallet_id}/confirmation/transaction/{transaction_id}",
            "edit": f"127.0.0.1:8000/wallets/{wallet_id}/send_by_phone_number?transaction_id={transaction_id}"
            }

        result = wallet_service.send_to_email("Credit", wallet_id, 1, email, amount)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user(self, mock_id_exists):
        mock_id_exists.return_value = False
        wallet_id = 1
        author_id = 5

        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_user(wallet_id, author_id, )

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_phone')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user_if_phone_number(self, mock_find_user_by_phone, mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_phone.return_value = None
        wallet_id = 1
        author_id = 5
        phone_number = "0891231234"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_user(wallet_id, author_id, phone_number=phone_number)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with phone number {phone_number} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_phone')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_add_user_if_phone_number(self, mock_db_insert_query, mock_find_user_by_phone,
                                                                                  mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_phone.return_value = [(7,)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        phone_number = "0891231234"

        wallet_service.add_user(wallet_id, author_id, phone_number=phone_number)

        expected = f"User with phone_number {phone_number} successfully added  to wallet with id {wallet_id}"
        result = wallet_service.add_user(wallet_id, author_id, phone_number=phone_number)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_username')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user_if_username(self, mock_find_user_by_username,
                                                                                  mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_username.return_value = None
        wallet_id = 1
        author_id = 5
        username = "gosho123"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_user(wallet_id, author_id, username=username)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with username {username} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_username')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_add_user_if_username(self, mock_db_insert_query, mock_find_user_by_username,
                                                             mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_username.return_value = [("tosho123",)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        username = "gosho123"

        wallet_service.add_user(wallet_id, author_id, username=username)

        expected = f"User {username} successfully added  to wallet with id {wallet_id}"
        result = wallet_service.add_user(wallet_id, author_id, username=username)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_email')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user_if_email(self, mock_find_user_by_email,
                                                                                  mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_email.return_value = None
        wallet_id = 1
        author_id = 5
        email = "gosho@abv.bg"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.add_user(wallet_id, author_id, email=email)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with email {email} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.find_user_by_email')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_add_user_if_email(self, mock_db_insert_query, mock_find_user_by_email,
                                                             mock_id_exists):
        mock_id_exists.return_value = True
        mock_find_user_by_email.return_value = [("gosho123@abv.bg",)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        email = "tosho@abv.bg"

        wallet_service.add_user(wallet_id, author_id, email=email)

        expected = f"User with email: {email} successfully added  to wallet with id {wallet_id}"
        result = wallet_service.add_user(wallet_id, author_id, email=email)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    def test_returns_correct_msg_when_no_input(self, mock_id_exists):
        mock_id_exists.return_value = True
        wallet_id = 1
        author_id = 5

        expected = "Something is wrong"
        result = wallet_service.add_user(wallet_id, author_id, )

        self.assertEqual(expected, result)



    # Revoke User Access

    @patch('services.wallet_service.id_exists')
    def test_raise_HTTPException_when_id_doesnt_exist_in_revoke_access(self, mock_id_exists):
        mock_id_exists.return_value = False
        wallet_id = 1
        author_id = 5

        with self.assertRaises(HTTPException) as exception:
            wallet_service.revoke_access(wallet_id, author_id)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Wallet with id {wallet_id} doesnt exist")

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    def test_raise_HTTPException_when_author_doesnt_exist_in_revoke_access(self, mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = None
        wallet_id = 1
        author_id = 5

        with self.assertRaises(HTTPException) as exception:
            wallet_service.revoke_access(wallet_id, author_id)

            self.assertEqual(exception.exception.status_code, 400)
            self.assertEqual(exception.exception.detail, "You are not the creator of this wallet")

    # IF Phone Number
    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_phone')
    def test_raise_HTTPException_when_id_doesnt_exist_in_revoke_access_if_phone_number(self, mock_find_user_by_phone,
                                                                                  mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 2
        mock_find_user_by_phone.return_value = None
        wallet_id = 1
        author_id = 5
        phone_number = "0891231234"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.revoke_access(wallet_id, author_id, phone_number=phone_number)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with phone number {phone_number} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_phone')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_revoke_access_if_phone_number(self, mock_db_insert_query, mock_find_user_by_phone,
                                                             mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 5
        mock_find_user_by_phone.return_value = [(7,)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        phone_number = "0891231234"

        wallet_service.add_user(wallet_id, author_id, phone_number=phone_number)

        expected = f"User with phone_number {phone_number} successfully deleted  to wallet with id {wallet_id}"
        result = wallet_service.revoke_access(wallet_id, author_id, phone_number=phone_number)

        self.assertEqual(expected, result)

    # If Username
    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_username')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user_if_username(self, mock_find_user_by_username,
                                                                              mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 7
        mock_find_user_by_username.return_value = None
        wallet_id = 1
        author_id = 5
        username = "gosho123"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.revoke_access(wallet_id, author_id, username=username)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with username {username} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_username')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_revoke_access_if_username(self, mock_db_insert_query, mock_find_user_by_username,
                                                         mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 7
        mock_find_user_by_username.return_value = [("tosho123",)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        username = "gosho123"

        wallet_service.revoke_access(wallet_id, author_id, username=username)

        expected = f"User {username} successfully deleted to wallet with id {wallet_id}"
        result = wallet_service.revoke_access(wallet_id, author_id, username=username)

        self.assertEqual(expected, result)


    # If Email
    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_email')
    def test_raise_HTTPException_when_id_doesnt_exist_in_add_user_if_email(self, mock_find_user_by_email,
                                                                              mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 7
        mock_find_user_by_email.return_value = None
        wallet_id = 1
        author_id = 5
        email = "gosho123@abv.bg"

        with self.assertRaises(HTTPException) as exception:
            wallet_service.revoke_access(wallet_id, author_id, email=email)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f'User with email {email} not found.')

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    @patch('services.wallet_service.find_user_by_email')
    @patch('services.wallet_service.insert_query')
    def test_returns_correct_msg_in_revoke_access_if_email(self, mock_db_insert_query, mock_find_user_by_email,
                                                      mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 7
        mock_find_user_by_email.return_value = [("gosho123@abv.bg",)]
        mock_db_insert_query.return_value = None
        wallet_id = 1
        author_id = 5
        email = "tosho@abv.bg"

        wallet_service.add_user(wallet_id, author_id, email=email)

        expected = f"User with email: {email} successfully deleted to wallet with id {wallet_id}"
        result = wallet_service.revoke_access(wallet_id, author_id, email=email)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.id_exists')
    @patch('services.wallet_service.read_query')
    def test_returns_correct_msg_in_revoke_access_when_no_input(self, mock_db_read_query, mock_id_exists):
        mock_id_exists.return_value = True
        mock_db_read_query.return_value = 7
        wallet_id = 1
        author_id = 5

        expected = "Something is wrong"
        result = wallet_service.add_user(wallet_id, author_id, )

        self.assertEqual(expected, result)

    @patch('services.wallet_service.query_count')
    def test_return_correct_bool_balance_sufficient_if_true(self, mock_db_query_count):
        mock_db_query_count.return_value = 5

        expected = True

        result = wallet_service.is_balance_sufficient(1000.0, 3)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.query_count')
    def test_return_correct_bool_balance_sufficient_if_false(self, mock_db_query_count):
        mock_db_query_count.return_value = None

        expected = False

        result = wallet_service.is_balance_sufficient(1000.0, 3)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_find_user_card_id(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = [5]

        expected = 5

        result = wallet_service.find_user(card_id=18)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_find_user_wallet_id(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = [5]

        expected = 5

        result = wallet_service.find_user(wallet_id=18)

        self.assertEqual(expected, result)

    def test_raise_HTTPException_when_ids(self):
        with self.assertRaises(HTTPException) as exception:
            wallet_service.find_user()

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, 'User does not exist.')

    @patch('services.wallet_service.query_count')
    def test_returns_true_is_users_wallet(self, mock_db_query_count):
        mock_db_query_count.return_value = 1

        expected = True

        result = wallet_service.is_users_wallet(5)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.query_count')
    def test_returns_false_is_users_wallet(self, mock_db_query_count):
        mock_db_query_count.return_value = None

        expected = False

        result = wallet_service.is_users_wallet(5)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_returns_correct_value_find_user_by_phone(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = 5,
        phone_number = "0891231234"

        expected = 5

        result = wallet_service.find_user_by_phone(phone_number)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_raise_HTTPException_when_not_receiver_id_find_user_by_phone(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = None
        phone_number = "0891231234"
        with self.assertRaises(HTTPException) as exception:
            wallet_service.find_user_by_phone(phone_number)

            self.assertEqual(exception.exception.status_code, 403)
            self.assertEqual(exception.exception.detail, f'User with phone {phone_number} not found.')

    @patch('services.wallet_service.read_query_one')
    def test_returns_correct_value_find_user_by_username(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = 5,
        username = "gosho123"

        expected = 5

        result = wallet_service.find_user_by_username(username)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_raise_HTTPException_when_not_receiver_id_find_user_by_username(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = None
        username = "gosho123"
        with self.assertRaises(HTTPException) as exception:
            wallet_service.find_user_by_username(username)

            self.assertEqual(exception.exception.status_code, 403)
            self.assertEqual(exception.exception.detail, f'User with username {username} not found.')

    @patch('services.wallet_service.read_query_one')
    def test_returns_correct_value_find_user_by_email(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = 5,
        email = "gosho123@abv.bg"

        expected = 5

        result = wallet_service.find_user_by_email(email)

        self.assertEqual(expected, result)

    @patch('services.wallet_service.read_query_one')
    def test_raise_HTTPException_when_not_receiver_id_find_user_by_email(self, mock_db_read_query_one):
        mock_db_read_query_one.return_value = None
        email = "gosho123@abv.bg"
        with self.assertRaises(HTTPException) as exception:
            wallet_service.find_user_by_email(email)

            self.assertEqual(exception.exception.status_code, 403)
            self.assertEqual(exception.exception.detail, f'User with email {email} not found.')

    @patch('services.wallet_service.read_query')
    def test_returns_correct_wallets_find_wallet_names(self, mock_db_read_query):
        mock_db_read_query.return_value = [("wallet_1",),("wallet_2",)]

        expected = ["wallet_1", "wallet_2"]

        result = wallet_service.find_wallet_names(5)

        self.assertEqual(expected,result)