import unittest
from unittest.mock import Mock, patch
from fastapi import HTTPException
from core.models.user import User
from core.utilities import get_current_time
from services import user_service

mock_wallet_service = Mock('src.services.wallet_service')
user_service.wallet_service = mock_wallet_service

class UserServiceGetAllUsersShould(unittest.TestCase):

    def test_get_all_registered_users_returnsAllUsersPresent(self):
        with patch('services.user_service.read_query') as mock_db_read_query:
                mock_db_read_query.return_value = [
                (1, 'John', 'password123', 'john@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1),
                (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1),
                (3, 'Tom Soyer', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1),
                ]
                expected = [
                User.from_query_result(*(1, 'John', 'password123', 'john@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
                User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
                User.from_query_result(*(3, 'Tom Soyer', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
                ]

                result = user_service.get_all_registered_users()
                print(result)
                self.assertEqual(expected, list(result))


    def test_get_all_registered_users_returnsSearchByUsername(self):
        with patch('services.user_service.read_query') as mock_db_read_query:
                mock_db_read_query.return_value = [
                (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1),
                ]
                expected = [
                User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
                ]
                result = user_service.get_all_registered_users('Alice', search_by='username')
                self.assertEqual(expected, list(result))


    def test_get_all_registered_users_returnsSearchByPhone(self):
        with patch('services.user_service.read_query') as mock_db_read_query:
                mock_db_read_query.return_value = [
                (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1),
                ]
                expected = [
                User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
                ]
                result = user_service.get_all_registered_users('12345', search_by='phone')
                self.assertEqual(expected, list(result))


    def test_get_all_registered_users_returnsSearchByEmail(self):
        with patch('services.user_service.read_query') as mock_db_read_query:
                mock_db_read_query.return_value = [
                (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1),
                ]
                expected = [
                User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
                ]
                result = user_service.get_all_registered_users('Alice@example.com', search_by='email')
                self.assertEqual(expected, list(result))


# class UserServiceGetByIdShould(unittest.TestCase):
#     def test_get_by_id_users_returnsUserById(self):
#         user_service.get_users_transactions_sender = lambda user_id: []
#         user_service.get_users_transactions_receiver = lambda user_id: []
#         user_service.wallet_service.find_user_wallet = lambda user_id, name: 'Gucci Wallet'
#         user_service.wallet_service.find_wallet_names = lambda user_id: 'Gucci Wallet'
#         with patch('services.user_service.read_query_one') as mock_db_read_query_one:
#                 mock_db_read_query_one.return_value = (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)
                
#                 expected = User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)) 
                
#                 result = user_service.get_by_id(2)

#                 result.default_wallet = user_service.wallet_service.find_user_wallet(result.id, name=True)
#                 result.transactions = []
#                 result.wallets = user_service.wallet_service.find_wallet_names(result.id)
#                 self.assertEqual(expected, list(result))


class CreateUserShould(unittest.TestCase):
    def test_create_user_returnsCreatedUser(self):
        with patch('services.user_service.insert_query') as mock_db_insert_query:
            mock_db_insert_query.return_value = 1
            new_user = user_service.create_user( User(
                                                        username='John Doe',
                                                        password='Passw0rd!',
                                                        email='test@email.com',
                                                        phone_number='0888456789',
                                                        is_blocked=False,
                                                        is_admin=False,
                                                        is_verified=False,
                                                        ))
            self.assertEqual(new_user.id, 1)
            self.assertEqual(new_user.username, 'John Doe')
            self.assertEqual(new_user.password, 'Passw0rd!')
            self.assertEqual(new_user.phone_number, '0888456789')            
            self.assertEqual(new_user.created_at, get_current_time())
            self.assertEqual(new_user.is_blocked, False)
            self.assertEqual(new_user.is_admin, False)
            self.assertEqual(new_user.is_verified, False)



class UpdateUserShould(unittest.TestCase):
     def test_update_user_returnsUpdatedUser(self):
        with patch('services.user_service.update_query') as mock_db_update_query:
            mock_db_update_query.return_value = True
            old_user = User(username='John Doe',
                            password='Passw0rd!',
                            email='test@email.com',
                            phone_number='0888456789',
                            is_blocked=False,
                            is_admin=False,
                            is_verified=False,
                            )
            new_user = User(username='John Doe',
                            password='updated_Passw0rd!',
                            email='updated_test@email.com',
                            phone_number='123456789',
                            is_blocked=False,
                            is_admin=False,
                            is_verified=False,
                            )
            
            updated_user =  user_service.update_user(new_user, old_user)

            self.assertEqual(updated_user.username, 'John Doe')
            self.assertEqual(updated_user.password, 'updated_Passw0rd!')
            self.assertEqual(updated_user.email, 'updated_test@email.com')
            self.assertEqual(updated_user.phone_number, '123456789')  


class GetUserByIdShould(unittest.TestCase):
     
     @patch('services.user_service.read_query_one')
     def test_get_user_by_id_returnsUser(self, mock_db_read_query_one):
                mock_db_read_query_one.return_value = (2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)
                
                expected = User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1))
                         
                result = user_service.get_user_by_id(2)
                self.assertEqual(expected, result)

     @patch('services.user_service.read_query_one')
     def test_get_user_by_id_raisesHTTException(self, mock_db_read_query_one):
            mock_db_read_query_one.return_value = False
            non_existing_user_id = -1
            with self.assertRaises(HTTPException) as exception:
                user_service.get_user_by_id(non_existing_user_id)
            exception = exception.exception    
            self.assertEqual(exception.status_code, 404)
            self.assertEqual(exception.detail, f'User with id {non_existing_user_id} not found.')


class SortUsersShould(unittest.TestCase):
    list_of_users = [
    User.from_query_result(*(1, 'John', 'password123', 'John@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
    User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
    User.from_query_result(*(3, 'Tom', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
    User.from_query_result(*(4, 'Jack', 'password123', 'Jack@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
    User.from_query_result(*(5, 'Gucci Boi', 'Passw0rd!', 'Gucci@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
    ]  


    def test_sort_users_returnsProperSortWhenReverseIsFalse(self):
        expected = [
            User.from_query_result(*(1, 'John', 'password123', 'John@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
            User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(3, 'Tom', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            User.from_query_result(*(4, 'Jack', 'password123', 'Jack@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(5, 'Gucci Boi', 'Passw0rd!', 'Gucci@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            ]  
        sorted_users =  user_service.sort_users(SortUsersShould.list_of_users, attribute='id', reverse=False)   

        self.assertEqual(expected, sorted_users)


    def test_sort_users_returnsProperSortWhenReverseIsTrue(self):
        expected = [
            User.from_query_result(*(5, 'Gucci Boi', 'Passw0rd!', 'Gucci@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            User.from_query_result(*(4, 'Jack', 'password123', 'Jack@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(3, 'Tom', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(1, 'John', 'password123', 'John@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
            ]  
        sorted_users =  user_service.sort_users(SortUsersShould.list_of_users, attribute='id', reverse=True)   

        self.assertEqual(expected, sorted_users)


    def test_sort_users_returnsWhenSortingByName(self):
        expected = [
            User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(5, 'Gucci Boi', 'Passw0rd!', 'Gucci@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            User.from_query_result(*(4, 'Jack', 'password123', 'Jack@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(1, 'John', 'password123', 'John@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
            User.from_query_result(*(3, 'Tom', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            ]  
        sorted_users =  user_service.sort_users(SortUsersShould.list_of_users, attribute='name', reverse=False)   

        self.assertEqual(expected, sorted_users)


    def test_sort_users_returnsWhenSortingByEmail(self):
        expected = [
            User.from_query_result(*(2, 'Alice', 'password123', 'Alice@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(5, 'Gucci Boi', 'Passw0rd!', 'Gucci@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            User.from_query_result(*(4, 'Jack', 'password123', 'Jack@example.com', '1234567890', '2023-01-03 21:21:21', True, False, True, 1)),
            User.from_query_result(*(1, 'John', 'password123', 'John@example.com', '1234567890', '2023-01-01 10:10:10', False, False, True, 1)),
            User.from_query_result(*(3, 'Tom', 'Passw0rd!', 'Tom@example.com', '0888999341', '2023-01-02 22:22:22', False, True, False, 1)),
            ]  
        sorted_users =  user_service.sort_users(SortUsersShould.list_of_users, attribute='email', reverse=False)   

        self.assertEqual(expected, sorted_users)



class DisplayFriendsShould(unittest.TestCase):
     
     def test_display_friends_raisesHTTExceptionWhenNoFriends(self):
        with patch('services.user_service.read_query') as mock_db_read_query:
            mock_db_read_query.return_value = None
            with self.assertRaises(HTTPException) as exception:
                user_service.display_friends(-1)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, 'Friend list is empty.')

          