import datetime
import unittest
from unittest.mock import patch
from fastapi import HTTPException
from core.models.card import Card
from services import card_service

FAKE_CARD_ID = -1
FAKE_USER_ID = -1
ENCRYPTED_LIST_OF_CARDS = [(1, b'gAAAAABkf4PLG7uHc56p2b1SRHYvfmjWQL04aqXAEVUupgZvgfgqVOp5XK_S2dxNwy0aU2_N3Ef7ZFjscehW8XA4FJ-S6igTOi8toJtn_MVA5RCzK5PiMpY=', '2025-05-01', 
                           b'gAAAAABkf4PL9bSiOGDksc9zIWZdEsnjtDnoHIqILtKl4spEF1DeF-woSBFJ9M2SRtxQdr3gMjihdMePyW4CCSBnUu1LoqV-6g==', 
                           b'gAAAAABkf4PLgPzrC6D1G7v0qGZzZc9nZcWvs3geZCIArFfVsc5TOaXM8dygXnCjYr9yu4vFYDKxTaVmD-PjPYogKKmzR7Jj5Q==', 5000, 'BGN', None),
                          (2, b'gAAAAABkf4PLB7fZHNxDdVG0w_COh3jIyZhpol0pSn8o6Gm3ydxp32fmgkN3ivNGk_WVsHcoV6YLYfkJDUuRDrHioDchS0VA0SIztViDO8JC36H11Pt3zT4=', '2025-05-02', 
                           b'gAAAAABkf4PL7UqL6kz25R7bNmGZp4OiCJzSikMgC4AtBofNR6Oq5O7G-Y5C10SNiuyszCU9Y0QNJdnM0tiaYM-plEwMxMtUmA==', 
                           b'gAAAAABkf4PLGloBu7F6tYDqpT_ObrZZAvz7nxqguXGlvcEW1JFPO4Z-MlY5j9ZW1tgDfGZECHLQYGDDAOGCqZzcePG5E7c1og==', 5000, 'BGN', None),
                          (3, b'gAAAAABkf4PLvHQX7O72RwE4tgzEeep3-0wl6VwzOjwI4HCYfb9s9K4T6hLad2Moe52LjWDMs6nIelHWdKPZMc2IYt1NYBGnD11G9t9-MbvP5Q8CmzZwAhg=', '2025-05-03', 
                           b'gAAAAABkf4PLLVwYVjOr0mg85cfcJa3p8Yn9fD9kbUQ7sDKvGkG0WZ8gp3LccPkTzoWv25oPllWIF3M-yg_Yjwj_S813_l0reA==', 
                           b'gAAAAABkf4PLA9wZ_oDIj8dVjmlChW42RwL8C6yrGvnf_pShe1X6t-dgaH9fDihxvOmkldvMYXIj0ZTn4drl63rIE9RaUgZUMw==', 5000, 'BGN', None),
                          (4, b'gAAAAABkf4PLiW1DOwP2UPiAOeosWESTXl_QAjWURChrrCbsWk4ZKdn0OrUUL1tqRLfzviHp82yAday8jZtF4nqx5OhzdTlIcU7XerzCsvELzhvkpro9xQQ=', '2025-05-04', 
                           b'gAAAAABkf4PLQoA7CkWEKoBPqSUJcdU1L3hv0ksN2AprVrEIWciXl4C_KIlplr0Ig-0MF-MZWzTW6QwN2kB6VPtUk1btO8ywQw==', 
                           b'gAAAAABkf4PLBl-T4u2hPc61TsmpQduGEWaYOlAmvj9t3PNJF5pDzty9vKE1RH84AbD5ZEqsp9h7xxK1L1xJDK-Gf0QIQj58iQ==', 50000, 'USD', None),
                          (5, b'gAAAAABkf4PLkktGs4bOSlkjTj-lt-4BWTyKNsxkLr_NpHoFuiTep9DCledEpmPn2X5mZoiTU7z9YN6T_zzep9B6XLeYJbCCnif_LSwjfxV6Pzftei5ErQk=', '2025-05-05', 
                           b'gAAAAABkf4PLVwHIrVj_Ym9phwjVFZEqDnjeTykSjVcT3rE1lc_ghietmd59JRJifNTNmTnWUrj63pW7f1qkiiLPfVuixahSlg==', 
                           b'gAAAAABkf4PLP_W7LKqJRSkJF7-J4F94I4xClu0ohUhbNKjTjAo0mzzE8VmDaHS7tbW_Q8rjtXPg9Y5kKmTI6D84pYBEccRsoA==', 5000, 'BGN', None),
                          ]

class SortCardsShould(unittest.TestCase):
    list_of_cards = [Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                     Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                     Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                     Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                     Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None))
                     ]
    

    def test_sort_cards_returnsSortedCardsInAscendingOrder(self):
        expected = [Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                    Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None))
                    ]
        
        result = card_service.sort_cards(SortCardsShould.list_of_cards)     
        
        self.assertEqual(expected, result)


    def test_sort_cards_returnsSortedCardsInDescendingOrder(self):
        expected = [Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                    Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                    ]

        result = card_service.sort_cards(SortCardsShould.list_of_cards,reverse=True)     
        
        self.assertEqual(expected, result)    


    def test_sort_cards_returnsSortedCardsByExpiration(self):

        expected = [Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                    Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None)),
                    ]

        result = card_service.sort_cards(SortCardsShould.list_of_cards, attribute='expiration', reverse=False)     
        
        self.assertEqual(expected, result)   


    def test_sort_cards_returnsSortedCardsByBalance(self):

        expected = [Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                    ]

        result = card_service.sort_cards(SortCardsShould.list_of_cards, attribute='balance', reverse=False)     
        
        self.assertEqual(expected, result)   


    def test_sort_cards_returnsSortedCardsByHolder(self):

        expected = [Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                    Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                    Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                    ]

        result = card_service.sort_cards(SortCardsShould.list_of_cards, attribute='holder', reverse=False)     
        
        self.assertEqual(expected, result)  


class IsCardExpiredShould(unittest.TestCase):
    @patch('services.card_service.read_query')
    def test_is_card_expired_raisesException(self, mock_db_read_query):
        mock_db_read_query.return_value = False
        with self.assertRaises(HTTPException) as exception:
            card_service.is_card_expired_from_db(-1)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, 'Card expiration not found.')


    @patch('services.card_service.read_query')
    def test_is_card_expired_returnsTrueWhenValidCard(self, mock_db_read_query):
            mock_db_read_query.return_value = [(datetime.date(2023, 5, 29),)]

            result = card_service.is_card_expired_from_db(1)

            self.assertTrue(result)


    @patch('services.card_service.read_query')
    def test_is_card_expired_returnsFalseWhenCardIsExpired(self, mock_db_read_query):
            mock_db_read_query.return_value = [(datetime.date(9999, 12, 31),)]

            result = card_service.is_card_expired_from_db(1)

            self.assertFalse(result)

class DeleteShould(unittest.TestCase):

    @patch('services.card_service.read_query')
    @patch('services.card_service.insert_query')
    def test_delete_returnsWhenCardIsRemovedSuccessfuly(self, mock_db_read_query, mock_db_insert_query):
        mock_db_read_query.return_value = [(FAKE_CARD_ID,)]

        mock_db_insert_query.return_value = True

        result = card_service.delete(21,21)

        self.assertEqual(result, f'Card with id {FAKE_CARD_ID} successfully removed')


    @patch('services.card_service.read_query')
    def test_delete_returnsProperWhenCardIsRemoved(self, mock_db_read_query):
        mock_db_read_query.return_value = None
        with self.assertRaises(HTTPException) as exception:
             card_service.delete(FAKE_CARD_ID, FAKE_CARD_ID)

        self.assertEqual(exception.exception.status_code, 404)
        self.assertEqual(exception.exception.detail, f"Card with id {FAKE_CARD_ID} doesn\'t exist")
        


    @patch('services.card_service.read_query')
    @patch('services.card_service.card_belongs_user')
    @patch('services.card_service.insert_query')
    def test_delete_returnsWhenCardIsRemovedSuccessfuly(self, mock_db_read_query, mock_card_belongs_user, mock_db_insert_query):
        mock_db_read_query.return_value = [(FAKE_CARD_ID,)]
        mock_card_belongs_user.return_value = False
        mock_db_insert_query.return_value = True
        with self.assertRaises(HTTPException) as exception:
             card_service.delete(FAKE_CARD_ID, FAKE_CARD_ID)

        self.assertEqual(exception.exception.status_code, 403)
        self.assertEqual(exception.exception.detail, f"You are trying to delete another user\'s card.")


class FindUserCardShould(unittest.TestCase):
     
     @patch('services.card_service.read_query_one')
     def test_find_user_card_raisesExceptionWhenNoCardFound(self, mock_db_read_query_one):
        with self.assertRaises(HTTPException) as exception:
            mock_db_read_query_one.return_value = None
            card_service.find_user_card(FAKE_CARD_ID)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, 'User has no cards registered.')



     @patch('services.card_service.read_query_one')
     def test_find_user_card_returnsCardIdWhenFound(self, mock_db_read_query_one):
            mock_db_read_query_one.return_value = (5,)
            self.assertEqual(card_service.find_user_card(FAKE_CARD_ID), 5)


class GetAllCardsShould(unittest.TestCase):
     
     @patch('services.card_service.read_query')
     def test_get_all_returnsAllAvailableCards(self, mock_db_read_query):
          mock_db_read_query.return_value = ENCRYPTED_LIST_OF_CARDS

          expected = [  Card.from_query_result(*(1, '1234 5678 9101 1121', '2025-05-01', 'John', '123', 5000, 'BGN', None)),
                        Card.from_query_result(*(2, '1234 5678 9101 1121', '2025-05-02', 'Alice', '123', 5000, 'BGN', None)),
                        Card.from_query_result(*(3, '1234 5678 9101 1121', '2025-05-03', 'Tom', '123', 5000, 'BGN', None)),
                        Card.from_query_result(*(4, '1234 5678 9101 1121', '2025-05-04', 'Gucci Boy', '123', 50000, 'USD', None)),
                        Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None)),
                        ]
          
          result = card_service.get_all()
          self.maxDiff = None
          self.assertEqual(expected, list(result))


     @patch('services.card_service.card_belongs_user')
     @patch('services.card_service.read_query')
     def test_get_by_id_returnsProperCard(self, mock_db_read_query, mock_card_belongs_user):
          mock_db_read_query.return_value = [ENCRYPTED_LIST_OF_CARDS[4],]
          mock_card_belongs_user.return_value = True
          
          expected = Card.from_query_result(*(5, '1234 5678 9101 1121', '2025-05-05', 'Blagoy', '123', 5000, 'BGN', None))
                        
          
          result = card_service.get_by_id(5, FAKE_USER_ID)
          
          self.assertEqual(expected, result)


          
     @patch('services.card_service.read_query')
     def test_get_by_id_raisesExceptionWhenUserHasNoCards(self, mock_db_read_query):
          mock_db_read_query.return_value = None
          with self.assertRaises(HTTPException) as exception:
            card_service.get_by_id(FAKE_CARD_ID, FAKE_USER_ID)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail, f"Card with id {FAKE_CARD_ID} doesnt exist")
          
         
     @patch('services.card_service.card_belongs_user')
     @patch('services.card_service.read_query')
     def test_get_by_id_raisesExceptionWhenOtherCard(self, mock_db_read_query, mock_card_belongs_user):
          mock_db_read_query.return_value = [ENCRYPTED_LIST_OF_CARDS[4],]
          mock_card_belongs_user.return_value = False
          with self.assertRaises(HTTPException) as exception:
            card_service.get_by_id(FAKE_CARD_ID, FAKE_USER_ID)

            self.assertEqual(exception.exception.status_code, 403)
            self.assertEqual(exception.exception.detail, 'You are trying to access another user\'s card.')
          

class RegisterUserShould(unittest.TestCase):
     @patch('services.card_service.insert_query')
     def test_register_returnsCreatedCard(self, mock_db_insert_query):
        mock_db_insert_query.return_value = 1
        

        new_card = card_service.register(Card.from_query_result(1,
                                                                '1234 1234 1234 1234',
                                                                '2025-04-05',
                                                                'John',
                                                                '123',
                                                                5000,
                                                                'BGN',
                                                                1),
                                                                1)
        self.assertEqual(new_card.id, 1)
        self.assertEqual(new_card.card_number, '1234 1234 1234 1234')
        self.assertEqual(new_card.expiration, datetime.date(2025, 4, 5))
        self.assertEqual(new_card.holder, 'John')
        self.assertEqual(new_card.cvv, '123')
        self.assertEqual(new_card.balance, 5000)
        self.assertEqual(new_card.currency, 'BGN')
        self.assertEqual(new_card.users_id, 1)
          

class DeleteShould(unittest.TestCase):
    @patch('services.card_service.read_query')
    @patch('services.card_service.card_belongs_user')
    @patch('services.card_service.insert_query')
    def test_delete_returnsWhenCardIsDeleted(self, mock_db_insert_query, mock_card_belongs_user, mock_db_read_query):
        mock_db_read_query.return_value = True
        mock_db_insert_query.return_value = None
        mock_card_belongs_user.return_value = True

        result = card_service.delete(FAKE_CARD_ID, FAKE_USER_ID)

        self.assertEqual(result, f"Card with id {FAKE_CARD_ID} successfully removed")


    @patch('services.card_service.read_query')
    @patch('services.card_service.card_belongs_user')
    def test_delete_returnsWhenCardIsNotUsers(self, mock_card_belongs_user, mock_db_read_query):
        mock_db_read_query.return_value = True
        mock_card_belongs_user.return_value = False
        with self.assertRaises(HTTPException) as exception:
            card_service.delete(FAKE_CARD_ID, FAKE_USER_ID)

            self.assertEqual(exception.exception.status_code, 403)
            self.assertEqual(exception.exception.detail, 'You are trying to access another user\'s card.')



    @patch('services.card_service.read_query')
    def test_delete_returnsWhenCardIsDeleted(self,  mock_db_read_query):
        mock_db_read_query.return_value = None
        with self.assertRaises(HTTPException) as exception:
            card_service.delete(FAKE_CARD_ID, FAKE_USER_ID)

            self.assertEqual(exception.exception.status_code, 404)
            self.assertEqual(exception.exception.detail,  f"Card with id {FAKE_CARD_ID} doesnt exist")


class UpdateCardShould(unittest.TestCase):
    @patch('services.card_service.update_query')
    def test_update_card_returnsUpdatedCard(self, mock_db_update_query):
        mock_db_update_query.return_value = None
        new_card = Card.from_query_result(1,
                                        '0000 0000 0000 0000',
                                        '2025-04-04',
                                        'Alice',
                                        '321',
                                        5000,
                                        'EUR',
                                        1,
                                        )
        old_card = Card.from_query_result(1,
                                        '1234 1234 1234 1234',
                                        '2025-04-05',
                                        'John',
                                        '123',
                                        5000,
                                        'BGN',
                                        1,
                                        )
        
        updated_card = card_service.update_card(new_card, old_card, FAKE_USER_ID)

        self.assertEqual(updated_card.id, new_card.id)
        self.assertEqual(updated_card.card_number, new_card.card_number)
        self.assertEqual(updated_card.expiration, new_card.expiration)
        self.assertEqual(updated_card.holder, new_card.holder)
        self.assertEqual(updated_card.cvv, new_card.cvv)
        self.assertEqual(updated_card.balance, new_card.balance)
        self.assertEqual(updated_card.currency, new_card.currency)
        self.assertEqual(updated_card.users_id, FAKE_USER_ID)