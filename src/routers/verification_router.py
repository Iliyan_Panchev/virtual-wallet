import datetime
from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates
from services import user_service


verification_router = APIRouter(prefix='/verification', tags=['Verification'])



@verification_router.get('/registration/{user_email}/t/{expiry}')
def verify_user(user_email: str, expiry: str, request: Request):
    if is_url_expired(expiry):
        return user_service.verification_url_expired_response(request)
    return user_service.verify_user_response(user_email, request)



@verification_router.get('/user/{user_email}/friend/{friend_email}/t/{expiry}')
def verify_friend_referral(user_email:str, friend_email: str, expiry, request: Request):
    if is_url_expired(expiry):
        return user_service.referral_url_expired_response(request)
    return user_service.referral_redirect(request, user_email)

@verification_router.get('/referral_redirect')
def redirected_referral():
    return user_service.refer_friend_response()


def is_url_expired(expiry_format: str):
    return datetime.datetime.now() > \
        datetime.datetime.strptime(expiry_format, "%Y-%m-%dT%H:%M:%S")

