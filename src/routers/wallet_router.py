from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Request
from core.auth import Oauth2
from services import wallet_service, card_service
wallet_router = APIRouter(prefix="/wallets",tags=['Wallet'])


@wallet_router.get("/")
def get_all_wallets(logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    
    return wallet_service.get_all()


@wallet_router.get("/{id}")
def get_wallet_by_id(id: int,
                     logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    wallet = wallet_service.get_by_id(id)
    return wallet


@wallet_router.post("/", status_code=201)
def create_wallet(currency: str, name:str,
                  logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    return wallet_service.create(currency, name, user_id)


@wallet_router.delete("/{id}")
def delete_wallet(id: int, 
                  logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    return wallet_service.delete(id)


# @wallet_router.post("/{wallet_id}card/{card_id}")
# def add_card_to_wallet(wallet_id: int, card_id: int):
#     return wallet_service.add_card_in_wallet(wallet_id, card_id)


@wallet_router.post("/{wallet_id}/users_access")
def add_user_to_wallet(wallet_id: int,
                       phone_number: str | None = None, 
                       username: str | None = None, email: str | None = None,
                       logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    return wallet_service.add_user(wallet_id, user_id, phone_number, username, email)


@wallet_router.delete("/{wallet_id}/users_access")
def revoke_user_from_wallet(wallet_id:int,
                            phone_number: str | None = None, 
                            username: str | None = None, email: str | None = None,
                            logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    return wallet_service.revoke_access(wallet_id, user_id, phone_number, username, email)


@wallet_router.post("/deposit")
def deposit(amount: float,
            id: int | None=None,
            card_id: int | None=None,
            logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
            ):
    if not card_id:
        card_id = card_service.find_user_card(logged_user.id)
    if id:
        if not wallet_service.is_users_wallet(logged_user.id):
            raise HTTPException(status_code=400, detail=f'The wallet is not associated with {logged_user.username}')
        wallet_id = id
    else:    
        wallet_id = wallet_service.find_user_wallet(logged_user.id)        
    return wallet_service.add_money_to_wallet(wallet_id, amount, card_id)


@wallet_router.post("/withdraw")
def withdraw(amount: float,
             id: int | None=None, 
            card_id: int | None=None, 
            logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    if not card_id:
        card_id = card_service.find_user_card(logged_user.id)
    if id:
        if not wallet_service.is_users_wallet(logged_user.id):
            raise HTTPException(status_code=400, detail=f'The wallet is not associated with {logged_user.username}')
        wallet_id = id
    else:    
        wallet_id = wallet_service.find_user_wallet(logged_user.id)        
    return wallet_service.remove_money_from_wallet(wallet_id, amount, card_id)


@wallet_router.get("/transfer")
def choose_wallet_to_send(logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    return wallet_service.check_wallet_access(user_id)


@wallet_router.post("/send_by_phone_number")
def send_by_phone_number(phone_number: str, amount: float, wallet_id: int | None=None, information: str='',  transaction_id: int | None = None,
                         logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    if not wallet_service.is_users_wallet(logged_user.id):
        raise HTTPException(status_code=400, detail='User has no wallet.')
    if wallet_id:
        if not wallet_service.is_users_wallet(logged_user.id):
            raise HTTPException(status_code=400, detail=f'The wallet is not associated with {logged_user.username}')
        wallet_id = wallet_id
    else:    
        wallet_id = wallet_service.find_user_wallet(logged_user.id)
    return wallet_service.send_to_phone_number(information, wallet_id, user_id, phone_number, amount, transaction_id)


@wallet_router.post("/send_by_username")
def send_by_username( username: str, amount: float, wallet_id: int | None=None,information: str='', transaction_id: int | None = None,
                     logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    if not wallet_service.is_users_wallet(logged_user.id):
        raise HTTPException(status_code=400, detail='User has no wallet.')
    if wallet_id:
        if not wallet_service.is_users_wallet(logged_user.id):
            raise HTTPException(status_code=400, detail=f'The wallet is not associated with {logged_user.username}')
        wallet_id = wallet_id
    else:    
        wallet_id = wallet_service.find_user_wallet(logged_user.id)
    return wallet_service.send_to_username(information, wallet_id, user_id, username, amount, transaction_id)


@wallet_router.post("/send_by_email")
def send_by_email( email: str, amount: float, wallet_id: int | None=None,information:str='', transaction_id: int | None = None,
                     logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    if not wallet_service.is_users_wallet(logged_user.id):
        raise HTTPException(status_code=400, detail='User has no wallet.')
    if wallet_id:
        if not wallet_service.is_users_wallet(logged_user.id):
            raise HTTPException(status_code=400, detail=f'The wallet is not associated with {logged_user.username}')
        wallet_id = wallet_id
    else:    
        wallet_id = wallet_service.find_user_wallet(logged_user.id)
    return wallet_service.send_to_email(information, wallet_id, user_id, email, amount, transaction_id)


@wallet_router.get("/{wallet_id}/confirmation/transaction/{transaction_id}")
def sender_confirmation(wallet_id: int, transaction_id: int):
    return wallet_service.send_confirmation(wallet_id, transaction_id)




@wallet_router.post("/confirm/verification_code")
def confirm_verification(transaction_id: int, verification_code: str):
    return wallet_service.confirm_verification_code(transaction_id, verification_code)