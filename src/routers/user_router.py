from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Request
from core.auth import Oauth2
from core.models.user import UpdateUser, UserTransactions, UserSearch, User, CreateUserModel, SelectPassword, UserSort
from core.utilities import get_page, paginate, create_hash, password_validator
from scripts.email_templates.user_verification import SUBJECT_VERIFY_USER, registration_verification_email
from scripts.email_templates.friend_referal import SUBJECT_FRIEND_REFERRAL, refer_friend_email
from services import user_service, email_service, wallet_service
from fastapi.security import OAuth2PasswordRequestForm





user_router = APIRouter(prefix='/user', tags=['User'])


@user_router.get('/')
def get_all_users(
    sort: str | None = None,
    sort_by: UserSort = Depends(),
    search_by: UserSearch = Depends(),
    search: str | None = None,
    size: int | None = None,
    page: int | None = None,
    logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
):
    if not user_service.is_admin(logged_user.id):
        raise HTTPException(status_code=403,detail='Only admins have access to all registered users.')
    all_users = user_service.get_all_registered_users(search, str(search_by))
    
    if sort and (sort == 'asc' or sort == 'desc'):
       all_users = user_service.sort_users(all_users, reverse=sort == 'desc', attribute=str(sort_by))
    if size:
       all_users = paginate(all_users, size)
    if page:
        all_users = get_page(all_users, page)
    return all_users 




@user_router.post('/register', status_code=201)
def register(user: CreateUserModel,
             background_tasks: BackgroundTasks, 
             ):
    if user_service.email_exists(user.email):
        raise HTTPException(status_code=400, detail=f'User with email: {user.email} already exists.')
    if user_service.phone_exists(user.phone_number):
        raise HTTPException(status_code=400, detail=f'User with phone number {user.phone_number} already exists.')
    if not password_validator(user.password):
            raise HTTPException(status_code=400, detail='Password must be at least 8 symbols and should contain capital letter, \
    digit and special symbol (+, -, *, &, ^, …)')
    user = User(username=user.username, 
                password=create_hash(user.password), 
                email=user.email, 
                phone_number=user.phone_number
                )
    background_tasks.add_task(email_service.send_email,
         SUBJECT_VERIFY_USER, 
         user.email, 
         registration_verification_email(user.username, user.email)
         )
    if email_service.user_is_referred(user.email):
        email_service.referral_bonus(wallet_service.find_user_wallet(
            email_service.user_is_referred(
            user.email)))
    return user_service.create_user(user)



@user_router.post('/token')
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
        return Oauth2.create_token(form_data)
    


@user_router.delete('/{user_email}')
def remove_user_from_platform(user_email: str, 
                              logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
                              ):
    if not user_service.is_admin(logged_user.id):
        raise HTTPException(status_code=403, detail='Only admins can remove users.')
    if not user_service.email_exists(logged_user.email):
        raise HTTPException(status_code=404, detail=f'User with email {user_email} does not exist.')
    return user_service.delete_user(user_email)


@user_router.post('/refer/{friend_email}')
def refer_friend(friend_email: str,
                 background_tasks: BackgroundTasks,
                 logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    wallet_service.find_user_wallet(logged_user.id)
    if not user_service.is_verified(logged_user.email):
        raise HTTPException(status_code=403,detail='Only verified users can send referral invites.')
    if user_service.email_exists(friend_email):
        raise HTTPException(status_code=400,detail=f'User is already registered with email {friend_email}')
    background_tasks.add_task(
        email_service.send_email, 
        SUBJECT_FRIEND_REFERRAL, 
        friend_email, 
        refer_friend_email(logged_user.email, friend_email) 
        )
    email_service.add_friend_referral(friend_email, logged_user.id)
    return {'message': f'Referral email sent to {friend_email}'}
    


@user_router.get('/register')
def register_redirect(request: Request):
    return user_service.refer_friend_response(request)


@user_router.get('/re-verify/{user_email}')
def resend_verification_email(user_email: str,
                 background_tasks: BackgroundTasks,
                 logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    
    background_tasks.add_task(
        email_service.send_email, 
        SUBJECT_VERIFY_USER, 
        user_email, 
        registration_verification_email(logged_user.username, logged_user.email))
    return {
        'message':'verification email re-sent.'
    }

@user_router.get('/profile')
def get_user_profile(
    transactions: UserTransactions = Depends(),
    logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):

    if not user_service.is_verified(logged_user.email):
        raise HTTPException(status_code=400, detail='Only verified users can view their profile.')
    return user_service.get_profile_by_id(logged_user.id, str(transactions))


@user_router.post('/add_friend')
def add_friend_list(logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
                    username: str | None=None,
                    phone_number: str | None=None,
                    email: str | None=None,
                    ):
    if username:
        return user_service.add_friend(logged_user.id, username=username)
    elif phone_number:
        return user_service.add_friend(logged_user.id, phone_number=phone_number)
    elif email:
        return user_service.add_friend(logged_user.id, email=email)


@user_router.get('/all_friends')
def display_friend_list(logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),):
    return user_service.display_friends(logged_user.id)


@user_router.delete('/remove_friend/{friend_username}')
def remove_friend_from_list(friend_username:str,
                            _: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),):
        return user_service.remove_friend(username=friend_username)


@user_router.put('/update')
def update_user_profile(new_user: UpdateUser,
                        logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
                        ):
    old_user = user_service.get_user_by_id(logged_user.id)
    new_user = User(username=old_user.username,
                    password=new_user.password,
                    email=new_user.email,
                    phone_number=new_user.phone_number)
    return user_service.update_user(new_user, old_user)