import datetime
from fastapi import APIRouter, HTTPException, Request, Depends
from core.auth import Oauth2
from core.models.card import Card, RegisterCard
from core.utilities import get_page, paginate
from services import card_service, user_service


card_router = APIRouter(prefix="/cards", tags=['Card'])


@card_router.get("/")
def get_all_cards(
    logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),
    sorting: str | None = None,
    sort_by: str | None = None,
    size:int | None = None,
    page:int | None = None,
    ):
    if not user_service.is_admin(logged_user.id):
        raise HTTPException(status_code=403, detail='Only admins can view all registered cards.')
    cards = card_service.get_all()
    if sorting and (sorting == 'asc' or sorting == 'desc'):
       cards = card_service.sort_cards(cards, reverse=sorting == 'desc', attribute=sort_by)
    if size:
       cards = paginate(cards, size)
    if page:
        cards = get_page(cards, page)
        
    return cards


@card_router.get("/{id}")
def get_card_by_id(id: int,
                   logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    card = card_service.get_by_id(id, logged_user.id)
    return card


@card_router.post("/register", status_code=201)
def register_card(registered_card: RegisterCard, 
                  logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    if datetime.date.today() > datetime.datetime.strptime(str(registered_card.expiration), "%Y-%m-%d").date():
        raise HTTPException(status_code=400, detail='Card is expired.')
    user_id = logged_user.id
    registered_card = Card(card_number=registered_card.card_number,
                expiration=registered_card.expiration,
                holder=registered_card.holder,
                cvv=registered_card.cvv,
                balance=registered_card.balance,
                currency=registered_card.currency)
    return card_service.register(registered_card, user_id)


@card_router.delete("/{id}")
def delete_card(id: int,
                logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    
    return card_service.delete(id, logged_user.id)


@card_router.put("/{id}")
def edit_card(id: int, new_card: Card, 
              logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user)):
    user_id = logged_user.id
    old_card = card_service.get_by_id(id, logged_user.id)
    card = card_service.update_card(new_card, old_card, user_id)
    return card
