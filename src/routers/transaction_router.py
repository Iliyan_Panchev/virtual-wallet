from fastapi import APIRouter, Depends, HTTPException, Request
from core.auth.jwt_bearer import JwtBearer
from core.models.transaction import TransactionFilter, TransactionSort
from core.utilities import check_token, get_page, paginate
from services import transaction_service, wallet_service, user_service
from core.auth import Oauth2


transaction_router = APIRouter(prefix='/transactions', tags=['Transactions'])

@transaction_router.get('/')
def get_available_transactions(
    logged_user: Oauth2.TokenInfo = Depends(Oauth2.get_current_user),    
    sorting: str | None = None,
    sort_by: TransactionSort = Depends(),
    filter_by: TransactionFilter = Depends(),
    filtering: str | None = None,
    size: int | None = None,
    page: int | None = None,
):
    if not user_service.is_admin(logged_user.id):
        raise HTTPException(status_code=403, detail='Only admins can access this information.')
    all_transactions = transaction_service.get_all_transactions(filtering, str(filter_by))
    if sorting and (sorting == 'asc' or sorting == 'desc'):
       all_transactions = transaction_service.sort_transactions(all_transactions, reverse=sorting == 'desc', attribute=str(sort_by))
    if size:
       all_transactions = paginate(all_transactions, size)
    if page:
        all_transactions = get_page(all_transactions, page)
        
    return all_transactions 


# @transaction_router.put("/{id}")
# def update_transaction(id: int, phone_number):
#     pass

@transaction_router.post('/recurring')
def create_recurring_transactions(recurring_period: int, amount: str, receiver_id, wallet_id: int | None=None, information: str | None=''):
    if wallet_id:
        return transaction_service.create_recurring_transaction(recurring_period, information, amount, receiver_id, wallet_id=wallet_id)

@transaction_router.get("/accept_transaction/{transaction_id}")
def accept_transaction(transaction_id: int):
    return wallet_service.accept_trans(transaction_id)


@transaction_router.get("/decline_transaction/{transaction_id}")
def decline_transaction(transaction_id: int):
    return wallet_service.decline_trans(transaction_id)
