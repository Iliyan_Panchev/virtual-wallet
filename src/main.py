import uvicorn
from fastapi import FastAPI
from routers.card_router import card_router
from routers.wallet_router import wallet_router
from routers.user_router import user_router
from routers.verification_router import verification_router
from routers.transaction_router import transaction_router
from services.transaction_service import scheduler


app = FastAPI(title='Virtual Wallet API', 
              description='''Project Description\n
              Virtual Wallet is a web application that enables you to continently manage your budget. 
              Every user can send and receive money (user to user) and put money in his Virtual Wallet (bank to app). ''',
              version=0.1,
              on_startup=[scheduler.start]
              )

app.include_router(card_router)
app.include_router(wallet_router)
app.include_router(user_router)
app.include_router(verification_router)
app.include_router(transaction_router)
    
 
 
if __name__ == '__main__':
    uvicorn.run('main:app', reload=True)
    
    
    
