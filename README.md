# E-Wallet Web App RESTful API

This is an e-wallet web application that allows users to manage their digital wallets, make transactions, and perform various financial operations.

## Features

### User Registration: Users can create an account by providing their basic information.
  #### Register
  ```
  http://127.0.0.1:8000/user/register
  ```
  #### Registration requirements:
 
   - Username (must be unique and between 2 and 20 symbols)
   - Password (must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …))
   - Email (must be valid email and unique in the system)
   - Phone (number must be 10 digits and unique in the system)
  Verification is sent to the registration email with conformation link upon clicking the link    the user is verified and can use additional features of the app.
### User Authentication: Users can log in securely using their credentials.
  #### Login
  ```
  http://127.0.0.1:8000/docs
  ```
  #### Login requirements:

   - Username
   - Password

### Recharge: Users can recharge their wallets using various payment methods.
  #### Register card:
  ```
  http://127.0.0.1:8000/cards/register
  ```

  Multiple Cards – A user can register multiple credit and or debit cards, from which to add funds to their accounts. When adding funds to their wallet, the user's card is automatically selected if there is no card registered exception is raised.

   - Logged and verified users can register a debit or credit card to their name.
   - Transfer card funds to e-wallet.

### Wallet Management: Users can create, view, and manage multiple wallets.
  #### Create Wallet
   ```
   http://127.0.0.1:8000/wallets
   ```
  #### Wallet creation requirements:
   - Wallet name (up to 45 characters long)
   - Wallet currency (BGN, USD, EUR)
  ```
  http://127.0.0.1:8000/wallets/deposit
  ```

   - Users can deposit money from their registered cards to their wallets. 
   - Deposit money is automatic with one required parameter which is the amount.
   - Default wallet is used for depositing and registered card with the most balance if none is selected.
### Transaction History: Users can view their transaction history, including details of each transaction.
  #### Entire transaction history
   ```
   http://127.0.0.1:8000/transactions/
   ```
Only available for users with admin rights.
  #### User's transactions:
  ```
  http://127.0.0.1:8000/user/profile
  ```
  - Available in his profile.
  - User's default wallet name is displayed at the bottom 
  - If user has more the one wallet they are displayed by their names in the wallets section.
  - Transaction history with the ability to filter incoming/outgoing transactions is available for each user.
  - The report is generated depending on the current logged user.
### Fund Transfer: Users can send money to other users within the platform.
#### Send money with just phone number:
 ```
 http://127.0.0.1:8000/wallets/send_by_phone_number
 ```
 - Input the desired user phone number and money is sent instantly!
 - Only amount and phone are needed as the default wallet is used for transacting in the app.
#### Send money with just username:
 ```
 http://127.0.0.1:8000/wallets/send_by_username
 ``` 
 - Input user's username 
 - No need to enter specific wallet
#### Send money with just email:
 ```
 http://127.0.0.1:8000/wallets/send_by_email
 ``` 
 - Input user's known email address
 - No need to specify wallet
### Balance Inquiry: Users can check their wallet balances.
#### User wallet balance and information:
 ```
 http://127.0.0.1:8000/wallets/{wallet_id}
 ```
 - Wallet name, balance, currency
 
### Security: The application ensures the security of user data and transactions through card information encryption and password hashing.
- Password hashing algorithm: bcrypt
- Encryption algorithm: Fernet
- Payment Gateway Integration: None
### User Administration: Admin users have additional privileges to manage user accounts and system settings.
- Users with admin rights can access all transactions and edit them.
- Admins can see all registered cards and edit them.
- Admins can block users from sending money inside the app.
- Admins can remove users from the platform.

## Technologies Used

- Front-end: HTML, CSS, JavaScript, Bootstrap 5, Chart.js, Jinja2 templating language
- Back-end: Python
- Database: MariaDB
- Authentication: JWT (JSON Web Tokens) through Oauth2


## Installation

1. Clone the repository:

```bash
https://gitlab.com/team-135562715/virtual-wallet.git
```

2. Install the requirements:

```
pip install -r src/requirements.txt
```

3. Run uvicorn server:
```
uvicorn main:app --reload --port 8000
```

4. Check out the online documentation:

```
http://127.0.0.1:8000/docs
```